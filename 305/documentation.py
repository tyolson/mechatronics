## @mainpage ME 305 Portfolio
#  \tableofcontents
#  This is the main page for Tyler Olson's ME 305 (Winter 2021) portfolio.  Further documentation can be accessed from
#  the sidebar or from the links in each assignment description below.
#
#  @section sec_lab Labs
#  @subsection sec_lab1 Lab0x01
#  The first lab in class we wrote a simple python program to generate the fibonacci number at a given index.
#  This was my first introduction to Doxygen.  The lab documentation can be accessed at \ref Lab0x01.
#
#  @subsection sec_lab2 Lab0x02
#  In the second lab, we started interacting with the nucleo and programming on embedded hardware.  I have had some
#  previous experience with programming Arduino microcontrollers for some small project, but this was my first
#  interaction with the micropython library.  In this lab we programmed the user LED to change sequences every time the
#  blue user button is pressed, cycling through different patterns.  The lab documentation can be accessed at
#  \ref Lab0x02.
#
#  @subsection sec_lab3 Lab0x03
#  Let's play Simon Says!  In this lab the user now interacts with the nucelo, playing a game of binary Simon Says.  The
#  user is prompted what to do on a terminal screen and follows along, trying to get 10 correct patterns in a row.
#  The lab documentation can be accessed at \ref Lab0x03.
#
#  @subsection sec_labFF Lab0xFF
#  The ME 305 final project is a motor controller that can follow a given reference profile.  This project was separated
#  into 4 parts, each with their own deliverables and challenges to present at the end of the week.  See the
#  \ref labFF_main page for more information, where each week of the lab is broken out further.
#
#  @section sec_hw Homeworks
#  Below are homeworks that have associated documentation.  All other homeworks were submitted via pdf and are not
#  included in this portfolio
#  @subsection sec_hw2 Homework0x02
#  See \ref Hw0x02 documentation.
#
#  @author Tyler Olson
#  @date March 18, 2021
#


##  @page labFF_main Lab0xFF
#  \tableofcontents
#
#  <a href="https://bitbucket.org/tyolson/mechatronics/src/main/labFF/"><u>Here</u></a> is the repository for the
#  complete final project and more detailed documentation is located at \ref Lab0xFF.
#  Some measured performance profiles for P-control (Week 3) and PI-control (Week 4) can be found in
#  their respective sub-directories.  The most important plots are provided on each week's deliverables page.
#  @section sec_FFwk1 Week 1
#  In part 1 we introduced serial communication and how to interact with the nucleo over multiple serial ports.
#
#  Part 1 details and deliverables: \ref labFF_week1.
#  @section sec_FFwk2 Week 2
#  In part 2 we developed an encoder driver for the encoders on the ME405 board on the backend and a user interface
#  on the front end to interact with those encoders.  This user interface was updated and used throughout the project.
#
#  Part 2 details and deliverables: \ref labFF_week2.
#  @section sec_FFwk3 Week 3
#  In part 3 we developed a DC motor driver for the motors on the ME405 board.  These motors are driven by a PWM
#  H-bridge circuit which controls one channel at a time for forwards or reverse motion.  Secondly, in this lab we
#  also designed a closed loop controller that can perform proportional control on the motor that can generate and
#  measure a step response.
#
#  Part 3 details and deliverables: \ref labFF_week3.
#  @section sec_FFwk4 Week 4
#  In the final week of this project, we changed the controller from a P-controller to a PI-controller and implemented
#  reference tracking.  Now the user can provide a reference profile for the board to follow and the nucleo will report
#  back how well it performed.
#
#  Part 4 details and deliverables: \ref labFF_week4.
#
#  @author Tyler Olson
#  @date March 18, 2021
#

## @page labFF_week1 Lab0xFF | Week 1
#
#  This week we extended the interface with the Nucleo to include Serial connection and Terminal connection.  Below is
#  a state diagram for the system.
#  @image html labFF_State_Diagram.jpg width=30%
#
#  @author Tyler Olson
#
#  @date February 24, 2021
#

## @page labFF_week2 Lab0xFF | Week 2
#
#  Here is a sample plot created by turning the encoder manually by hand.  A .csv file was also generated, containing
#  each data point which can be accessed
#  <a href="https://bitbucket.org/tyolson/mechatronics/src/main/labFF/Week1/data2021_02_27_06-24-23_PM.csv">
#  <u>here</u></a>.
#  @image html plot2021_02_27_06-24-23_PM.png width=30%
#
#  @author Tyler Olson
#
#  @date February 27, 2021
#

## @page labFF_week3 Lab0xFF | Week 3
#
#  This week, we developed a motor driver and proportional controller.  The proportional gain value was initially
#  calculated by hand to be Kp=0.470[%/RPM] which, surprisingly, was remarkably close to the final value chosen after
#  testing Kp=0.48.  Calculations for both P-controller and PI-controller gains can be accessed
#  <a href="https://bitbucket.org/tyolson/mechatronics/src/main/labFF/Calculations.pdf">
#  <u>here</u></a>.
#
#  Values of 0.1-0.6 were tested in increments of 0.1.  Upon hitting an upper limit for stability at Kp=0.5,
#  gain values were tested from 0.41-0.49 in increments of 0.01.  All plots are available in the code repository,
#  however below are a few important ones.  In order they are Kp=0.470 (initial test), Kp=0.01, Kp=0.6, Kp=0.48
#  @image html Profile_plot2021_03_17_10-49-32_PM.png width=30%
#  @image html Profile_plot2021_03_17_10-10-25_PM.png width=30%
#  @image html Profile_plot2021_03_17_10-13-50_PM.png width=30%
#  @image html Profile_plot2021_03_17_10-54-16_PM.png width=30%
#
#  @author Tyler Olson
#
#  @date March 17, 2021
#

## @page labFF_week4 Lab0xFF | Week 4
#
#  For the final week of this project, the controller was updated from a P-controller to a PI-controller for improved
#  reference tracking.  Additionally the frontend can now deliver a continuous stream of target velocities and positions
#  for the nucleo to track, and the nucleo will periodically (around every 50ms) report its position and velocity.
#  Reported data is compared to the reference values in order to obtain a score for how well the controller is working.
#  This score is based on the average variance.  Although I did some extensive testing (including the Ziegler-Nichols
#  heuristic method and various forms of guess-and-check), I was unable to conclusively
#  reach controller gains that had good position tracking and low velocity overshoot.  Because of this I chose to go
#  ahead and just use the calculated values, as they produced relatively good results.
#  Calculations for both P-controller and PI-controller gains can be accessed
#  <a href="https://bitbucket.org/tyolson/mechatronics/src/main/labFF/Calculations.pdf">
#  <u>here</u></a>.
#
#  Here is the final plot:
#  @image html Profile_plot2021_03_18_08-45-00_PM.png width=50%
#
#  @author Tyler Olson
#
#  @date March 18, 2021
#
