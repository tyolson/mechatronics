from pyb import UART
pyb.repl_uart(None)

if __name__ == "__main__":

    myuart = UART(2)
    while True:
        if myuart.any() != 0:
            val = myuart.readchar()
            myuart.write('You sent and ASCII ' + str(val) + ' to the Nucleo')
            print(str(val))

