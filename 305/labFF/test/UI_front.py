import serial

USB_VCP = "/dev/serial/by-id/usb-MicroPython_Pyboard_Virtual_Comm_Port_in_FS_Mode_207533885752-if01"

ST_LINK = "/dev/serial/by-id/usb-STMicroelectronics_STM32_STLink_066EFF565653756687202019-if02"

ser = serial.Serial(port=ST_LINK, baudrate=115273, timeout=1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval


if __name__ == "__main__":
    for n in range(1):
        print(sendChar())

    ser.close()
