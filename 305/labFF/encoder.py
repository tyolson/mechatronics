## \addtogroup Lab0xFF
#  @{
#  @package encoder
#

""" @file encoder.py
    @brief Class definition for an Encoder
    @author Tyler Olson
    @date February 25, 2021
"""

import micropython
from pyb import Timer, Pin
from math import pi


class Encoder:
    """
    @brief A Quadrature Encoder class to be used with the Nucleo-L476RG
    @details Supports encoder polling at a frequency of up to 65536pps
    """

    def __init__(self, ch_A, ch_B, timer: Timer, position=0):
        """
        @brief Constructor for the Encoder object
        @param ch_A Pin on channel A
        @param ch_B Pin on channel B
        @param timer A Timer that will be used to track the encoder
        @param position An optional initial position
        """
        self.ch_A = ch_A                                ##< Channel A #
        self.ch_B = ch_B                                ##< Channel B #
        self.enc = timer.channel(1, mode=Timer.ENC_AB)  ##< The timer channel to poll which collects encoder counts #
        self.timer = timer                              ##< The Timer that tracks the encoder #

        # Positions in Encoder Pulses
        self.last_count = 0                             ##< The previous pulse count #
        self.position = position                        ##< The current abosolute position as a count of pulses #

        ## The pulse rate (Pulses Per Revolution).
        #  The ME405 board uses US Digital E4T-1000 Encoders.
        #
        self.PPR = 1000 * 4

    def update(self, auto=None):
        """
        @brief Update the current position and the last position.
        @details Update the current position and the last position.  Assumes that the encoder is updated at least once
                 every half period of the timer and handles the overflow associated.  Thus any counter value above half
                 the period is assumed to be a backwards rotation.
        """

        # Poll Counter
        counts = self.timer.counter()
        delta = counts - self.last_count

        # Update current position
        period = self.timer.period()
        if delta > period / 2:
            delta -= period
        elif delta < -period / 2:
            delta += period
        self.position += delta

        # Update the previous encoder count
        self.last_count = counts

        # Start automatic update
        if auto is not None:
            micropython.schedule(self._auto_update, auto)

    def get_position(self, units="pulses"):
        """
        @brief Get the current encoder position
        @return Return the current position in the specified units.  If none are specified (or they are unrecognized)
                return the current position in pulses.
        @param units The units to return. Supported units:
                     * pules - The number of edges (timer counts)
                     * cycles - The number of cycles for a quadrature encoder (cycles = pulses / 4)
                     * degrees - The total angle in degrees
                     * radians - The total angle in degrees
                     * revolutions - The total number of revolutions
        """

        if units == "cycles":
            return self.position / 4
        elif units == "degrees":
            return self.position / self.PPR * 360
        elif units == "radians":
            return self.position / self.PPR * 2 * pi
        elif units == "revolutions":
            return self.position / self.PPR

        # else return the position in pules (timer counts)
        return self.position

    def set_position(self, position, units="pulses"):
        """
        @brief Set the current encoder position
        @return Return the current position in the specified units.  If none are specified (or they are unrecognized)
                return the current position in pulses.
        @param position The new value of the encoder position.
        @param units The units to return. Supported units:
                     * pules - The number of edges (timer counts)
                     * cycles - The number of cycles for a quadrature encoder (cycles = pulses / 4)
                     * degrees - The total angle in degrees
                     * radians - The total angle in degrees
                     * revolutions - The total number of revolutions
        """

        if units == "cycles":
            position *= 4
        elif units == "degrees":
            position *= self.PPR / 360
        elif units == "radians":
            position *= self.PPR / (2 * pi)
        elif units == "revolutions":
            position *= self.PPR

        # else return the position in pules (timer counts)
        self.position = position

    def get_delta(self):
        """
        @brief Get the change in encoder position (in pulses) since last update.
        """
        return self.timer.counter() - self.last_count


if __name__ == '__main__':
    B6 = Pin("B6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    B7 = Pin("B7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    C6 = Pin("C6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM3)
    C7 = Pin("C7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM3)
    enc1 = Encoder(B6, B7, Timer(4, prescaler=0, period=0xFFFF))
    enc2 = Encoder(C6, C7, Timer(3, prescaler=0, period=0xFFFF))

    print("Testing Encoders:")
    while True:
        enc1.update()
        enc2.update()
        print("\rEncoder 1: {:9.2} Encoder 2: {:9.2}".format(enc1.get_position(), enc2.get_position()), end="")

## @}
#
