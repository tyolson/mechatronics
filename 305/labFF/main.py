## \addtogroup Lab0xFF Lab0xFF
#  @{

""" @file main.py
    @brief Nucleo entry point.  Handles data transfer.
    @author Tyler Olson
    @date March 17, 2021
"""

import pyb
import utime
import array
import micropython
import motor
from math import exp, sin, pi
from pyb import Timer, Pin
from encoder import Encoder
from serial_codes import *
from closed_loop import ClosedLoop

_DEBUG = True
if _DEBUG:
    micropython.alloc_emergency_exception_buf(100)

## Data Collection period (ms)
#
COLLECTION_PERIOD = 50

## Data Collection timeout (s)
#
COLLECTION_TIMEOUT = 30


class State:
    """
    @brief The current state of the board.
    """

    COLLECTING = False                  ##< Indicates if the nucleo is collecting data or not #
    epoch = utime.ticks_ms()            ##< The time that controller was initiated #
    last = utime.ticks_ms()             ##< The time that the last control command was recieved #

    # Encoders
    B6 = Pin("B6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    B7 = Pin("B7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    enc1 = Encoder(B6, B7, Timer(4, prescaler=0, period=0xFFFF))

    # C6 = Pin("C6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8)
    # C7 = Pin("C7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8)
    # enc2 = Encoder(C6, C7, Timer(8, prescaler=0, period=0xFFFF))
    encoders = [enc1]                   ##< A list of all the encoders managed by the finite state machine #

    # Motors
    pin_nSLEEP = Pin("A15", mode=Pin.OUT_PP)
    pin_IN1 = Pin("B4", mode=Pin.OUT_PP)
    pin_IN2 = Pin("B5", mode=Pin.OUT_PP)
    timer = Timer(3, freq=20000)

    M1 = motor.Motor(pin_nSLEEP, pin_IN1, pin_IN2, timer)
    M1.enable()

    # pin_nSLEEP = Pin("A15", mode=Pin.OUT_PP)
    # pin_IN3 = Pin("B0", mode=Pin.OUT_PP)
    # pin_IN4 = Pin("B1", mode=Pin.OUT_PP)
    #
    # M2 = Motor(pin_nSLEEP, pin_IN3, pin_IN4, timer)
    # M2.enable()
    motors = [M1]                       ##< A list of all the motors managed by the finite state machine #

    # Closed-Loop Proportional controller
    controller = ClosedLoop(M1, enc1)   ##< The closed-loop (PI) controller that controls motor M1 and reads from encoder enc1 #

    @staticmethod
    def _schedule_update(irq_src: int):
        """
        @brief Handle interrupt and schedule an update to all present encoders and controllers.
        """
        try:
            micropython.schedule(State._update, None)
        except RuntimeError:
            pass

    @staticmethod
    def update(_):
        """
        @brief Update all current encoders and controllers.
        @param _ (This is just a placeholder, it serves no function)
        """
        for encoder in State.encoders:
            encoder.update()
        State.controller.update()

    @staticmethod
    def has_data():
        """
        @brief Check if it is time to report a data point.
        @details First checks if data should be sent.  Then checks if enough time has passed to collect and send the
                 next data point (specified by the COLLECTION_PERIOD).  Also checks that the COLLECITON_TIMEOUT has not
                 been reached.
        """
        if not State.COLLECTING:
            return False

        now = utime.ticks_ms()

        # check if the timeout has been reached if data is being collected
        if utime.ticks_diff(now, State.epoch) / 1000 > COLLECTION_TIMEOUT:
            State.COLLECTING = False
        elif utime.ticks_diff(now, State.last) > COLLECTION_PERIOD:
            State.last = now
            return True
        return False

    _update = update


def main():
    """
    @brief The entry point for the back-end interaction with the Nucleo.
    @details Starts collecting data as defined in generate() when a start signal is received on the UART serial line.
             Stops data collection when a stop signal is received.  Sends data back to the frontend on the UART serial
             line as an array of bytes.  Data points are encoded as text in the form
             (time, velocity, position) = "#.#, #.#, #.#", separated by newlines.  Data is collected once every 50ms.
             If following a reference profile, targets are received synchronously, however data points may be reported
             asynchronously.  Targets for motor control are handled as frequently as possible.
    """
    # Stop sending debug data to UART
    pyb.repl_uart(None)
    serial = pyb.UART(2)

    # Initialize a variable for referencing during data collection
    epoch = None
    command_buffer = ""

    # Start 250Hz timer to update the speed and position
    timer = Timer(1, freq=250, callback=State._schedule_update)

    # Start Finite State Machine
    while True:
        # Write a new data point if ready
        if _DEBUG:
            print("\rEncoder 1: pos {:9}|{:<9}[deg], speed {:9.4}|{:<9.4}[RPM]".format(
                int(State.enc1.get_position('degrees')), State.controller.target_position,
                State.controller.velocity, State.controller.target_speed), end="")

        # Write data if it exits
        if State.has_data():
            serial.write(generate(utime.ticks_diff(utime.ticks_ms(), epoch) / 1000, State.controller))

        # Skip if no input
        if serial.any() == 0:
            continue
        if _DEBUG:
            print("")

        # Poll the serial line
        buffer = command_buffer + serial.read().decode('ascii')
        command_buffer = buffer[buffer.rfind('\n') + 1:]
        commands = buffer[:buffer.rfind('\n') + 1]
        # if _DEBUG:
        #     print("Received command(s):", buffer)

        # First check if data collection must be stopped
        if State.COLLECTING and check_for_command(STOP_DATA_COLLECTION, commands):
            State.COLLECTING = False
            if _DEBUG:
                print("Data collection completed")
            continue

        # Then see if speed or position targets exist (if so, get the last ones)
        if check_for_command(SET_TARGET_SPEED, commands):
            c = commands[commands.rfind(SET_TARGET_SPEED.decode('ascii')):].partition('\n')[0]
            target_speed = float(c[len(SET_TARGET_SPEED):])
            State.controller.run(target_speed=target_speed)

        if check_for_command(SET_TARGET_POSITION, commands):
            c = commands[commands.rfind(SET_TARGET_POSITION.decode('ascii')):].partition('\n')[0]
            target_position = float(c[len(SET_TARGET_POSITION):])
            State.controller.run(target_position=target_position)

        # Finally check for these other commands
        if check_for_command(START_DATA_COLLECTION, commands):
            # collect from encoder 1
            if _DEBUG:
                print("Data collection started")
            epoch = utime.ticks_ms()
            State.last = epoch
            State.epoch = epoch
            State.COLLECTING = True

        # Other Misc Commands
        if check_for_command(ZERO_POSITION, commands):
            # Zero encoder 1 position
            State.controller.stop()
            State.enc1.set_position(0)

        if check_for_command(REPORT_POSITION, commands):
            # Report encoder 1 position
            serial.write(str(State.enc1.get_position()).encode('ascii'))

        if check_for_command(SET_KP, commands):
            if _DEBUG:
                old_Kp = State.controller.get_Kp()
            c = commands[commands.rfind(SET_KP.decode('ascii')):].partition('\n')[0]
            State.controller.set_Kp(float(c[len(SET_KP):]))
            if _DEBUG:
                print("Kp changed from {} to {}".format(old_Kp, State.controller.get_Kp()))

        if check_for_command(SET_KI, commands):
            if _DEBUG:
                old_Ki = State.controller.get_Ki()
            c = commands[commands.rfind(SET_KI.decode('ascii')):].partition('\n')[0]
            State.controller.set_Ki(float(c[len(SET_KI):]))
            if _DEBUG:
                print("Ki changed from {} to {}".format(old_Ki, State.controller.get_Ki()))


def generate(time_s, controller):
    """
    @brief Generates a data point based on the current time.
    @param time_s The current time in seconds
    @param controller A ClosedLoop controller to poll data from
    @return Returns a datapoint (time[s], position[deg], velocity[RPM]) as an array of bytes
            of the form "#.#, #.#, #.#\n"
    """

    csv_entry = "{}, {}, {}\n".format(time_s, controller.encoder.get_position('degrees'), controller.velocity)
    return csv_entry.encode('ascii')


def check_for_command(command, buffer):
    """
    @brief Checks if a certain command was read on the serial input line.
    @param command The command to search for
    @param buffer The serial buffer, as a string
    """
    return command.decode('ascii') in buffer


if __name__ == '__main__':
    main()

#  @}
#
