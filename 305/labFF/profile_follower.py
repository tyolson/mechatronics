## \addtogroup Lab0xFF
#  @{
#  @package ProfileFollower

""" @file profile_follower.py
    @brief A class for processing a reference profile and sending new velocity and speed targets as necessary
    @author Tyler Olson
    @date March 17, 2021
"""
import csv
import time
from serial_codes import *


class ProfileFollower:
    """
    @brief An object for processing a reference profile.
    @details This object both reads in a reference profile from a csv file and sends target speed and position to the
             nucleo when it is trying to follow the profile.
    """

    def __init__(self, serial, filename):
        """
        @brief A constructor for a ProfileFollower object.
        @details a ProfileFollower object can be constructed and then reused many times.
        @param serial The UART serial line on which to send profile targets to the nucleo.
        @param filename The file which contains the reference profile.  This must be a csv with floating-point entries
               separated by newlines.  Expects that each row is formatted as "time,velocity,position\n".
        """
        self.serial = serial            ##< A pyb.UART serial line to write targets to #
        self.targets = []               ##< An array containing profile targets spaced apart by 1ms #
        self.profile = filename         ##< The file that contains the reference profile #

        ## A boolean that indicates if the profile is currently being followed or not.
        #  This is set to False once the end of the profile has been reached.
        #
        self.is_collecting = False

        self.finished = False           ##< Indicates if the end of the reference profile has been reached #
        self.epoch = time.time()        ##< The start time of a collection run #

    def start(self):
        """
        @brief Start a collection run.
        @details If the target profile has not yet been imported, read it.  Then tell the nucleo to start data
                 collection and send the first reference target (at time 0ms).  This also resets the ProfileFollower
                 so that successive calls to write_target() will send the appropriate reference target (the target at
                 the current time) when it is called.
        """

        # collect the profile (if it hasn't been)
        if len(self.targets) == 0:
            with open(self.profile, "r") as file:
                reader = csv.DictReader(file, ["time", "velocity", "position"])
                for row in reader:
                    self.targets.append(row)

        # Reset Counters
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        self.serial.write(ZERO_POSITION)

        # Send the start code
        self.is_collecting = True
        self.serial.write(START_DATA_COLLECTION)
        self.epoch = time.time()

        # Send the first target
        self.write_target()

    def write_target(self):
        """
        @brief Send the next profile target to the nucleo based on the time since start() was called.
        """
        time_ms = int((time.time() - self.epoch) * 1000)
        if time_ms >= len(self.targets):
            self.stop()
            return

        # flush any output
        self.serial.reset_output_buffer()

        self.serial.write(SET_TARGET_SPEED)
        self.serial.write((self.targets[time_ms]["velocity"] + '\n').encode('ascii'))
        self.serial.write(SET_TARGET_POSITION)
        self.serial.write((self.targets[time_ms]["position"] + '\n').encode('ascii'))

    def stop(self):
        """
        @brief Stop collecting, and tell the nucleo to stop collecting and zero itself.
        """
        self.is_collecting = False
        self.finished = True

        # Send the stop code and collect last data points
        self.serial.write(STOP_DATA_COLLECTION)
        self.serial.write(ZERO_POSITION)


if __name__ == '__main__':
    pass

## @}
#
