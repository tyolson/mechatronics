## \addtogroup Lab0xFF
#  @{

""" @file serial_codes.py
    @brief Definitions for all of the serial line codes to be passed back and forth between the frontend and the nucleo.
    @author Tyler Olson
    @date March 18, 2021
"""

## Defines a constant for data collection start
#
START_DATA_COLLECTION = "START_DATA_COLLECTION\n".encode("ascii")

## Defines a constant for data collection stop
#
STOP_DATA_COLLECTION = "STOP_DATA_COLLECTION\n".encode("ascii")

## Defines a constant to set the current position to 0
#
ZERO_POSITION = "ZERO_POSITION\n".encode("ascii")

## Defines a constant to report the current position
#
REPORT_POSITION = "REPORT_POSITION\n".encode("ascii")

## Defines a constant for setting controller proportional gain
#
SET_KP = "SET_KP ".encode("ascii")

## Defines a constant for setting controller integrating gain
#
SET_KI = "SET_KI ".encode("ascii")

## Defines a constant for setting controller target speed
#
SET_TARGET_SPEED = "SET_TARGET_SPEED ".encode("ascii")

## Defines a constant for setting controller target position
#
SET_TARGET_POSITION = "SET_TARGET_POSITION ".encode("ascii")

## @}
#
