## \addtogroup Lab0xFF
#  @{
#  @package motor
#

""" @file motor.py
    @brief Class definition for a Motor object
    @author Tyler Olson
    @date March 18, 2021
"""

from pyb import Pin, Timer

## Set a limit for the board for maximum number of channels
#
MAX_CHANNEL_NUM = 4


class Motor:
    """
    @brief This class implements a motor driver for the ME405 board.
    """

    def __init__(self, pin_enable, pin_ch1, pin_ch2, timer):
        """
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param pin_enable A pyb.Pin object to use as the enable pin.
        @param pin_ch1 A pyb.Pin object to use as the input to half bridge 1.
        @param pin_ch2 A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        """
        self.timer = timer          ##< A timer used to control the motor duty cycle with PWM #
        self.IN1 = pin_ch1          ##< Channel IN1 on the ME405 board #
        self.IN2 = pin_ch2          ##< Channel IN2 on the ME405 board #
        self.nSLEEP = pin_enable    ##< An enable/disable pin on the ME405 board #

        # Get the next two available channels
        i = 1
        while timer.channel(i) is not None:
            if i > MAX_CHANNEL_NUM - 1:
                raise ValueError("Not enough free channels on this timer")
            i += 1

        # Set PWM channels, default to 0%
        self.ch1 = timer.channel(i, Timer.PWM, pin=self.IN1, pulse_width_percent=0)     ##< PWM channel A #
        self.ch2 = timer.channel(i+1, Timer.PWM, pin=self.IN2, pulse_width_percent=0)   ##< PWM channel B #

    def enable(self):
        """
        @brief Enable the motor.
        @details Caution: this method will enable any motors attached to its enable pin.
        """
        self.nSLEEP.high()

    def disable(self):
        """
        @brief Disable the motor and set its speed to zero.
        @details Caution: this method will disable any motors attached to its enable pin.
        """
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        self.nSLEEP.low()

    def set_duty(self, duty):
        """
        @details This method sets the duty cycle to be sent to the motor to the given level. Positive values cause
                 effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        """
        if duty > 0:
            self.ch1.pulse_width_percent(duty)
            self.ch2.pulse_width_percent(0)
        else:
            self.ch2.pulse_width_percent(duty * -1)
            self.ch1.pulse_width_percent(0)


if __name__ == '__main__':
    # Motor 1
    pin_nSLEEP = Pin("A15", mode=Pin.OUT_PP)
    pin_IN1 = Pin("B4", mode=Pin.OUT_PP)
    pin_IN2 = Pin("B5", mode=Pin.OUT_PP)
    timer = Timer(3, freq=20000)

    M1 = Motor(pin_nSLEEP, pin_IN1, pin_IN2, timer)
    M1.enable()

    # Motor 2
    pin_nSLEEP = Pin("A15", mode=Pin.OUT_PP)
    pin_IN3 = Pin("B0", mode=Pin.OUT_PP)
    pin_IN4 = Pin("B1", mode=Pin.OUT_PP)

    M2 = Motor(pin_nSLEEP, pin_IN3, pin_IN4, timer)
    M2.enable()

    # Prompt the user for a new duty cycle
    while True:
        M1.set_duty(int(input("Set Motor 1 PWM: ")))
        # M2.set_duty(int(input("Set Motor 2 PWM: ")))

## @}
#