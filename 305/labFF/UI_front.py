## \addtogroup Lab0xFF
#  @{

""" @file UI_front.py
    @brief Contains the front-end User Interface for interacting with the Nucleo.
    @details Currently only supported on Linux environments (64-bit Ubuntu 20.04.1)
    @author Tyler Olson
    @date March 18, 2021
"""

import re
import sys
import select
from datetime import datetime
from serial_codes import *

from matplotlib import pyplot
from serial import Serial
from profile_follower import ProfileFollower

_UI_DEBUG = True


def main(args):
    """
    @brief Start and run the UI frontend
    @details Prompts the user to press ‘G,’ to begin sampling data. Sends a start message to the Nucleo.
             Data collection will stop after 30s or at any time by pressing ‘S.’ When data collection is complete,
             generates both an image (using matplotlib) and a .CSV file of data (timestamps and data readings).
             Image and Data files are timestamped.
             z - Zero the encoder 1 position and stop motor.
             p - Print out the encoder 1 position.
             k - Set the Kp value.
             i - Set the Ki value.
             n - Set motor speed.
             r - Run a predefined profile and plot performance.
             s - End data collection prematurely.
    """

    # Define Serial port as ST-LINK
    print(args[0])
    serial = Serial(port=args[1], baudrate=115273, timeout=1)

    # Start infinite Loop.
    while True:
        usr_string = input("Enter a command [zpkinr]: ")
        if usr_string == 'z':
            serial.write(ZERO_POSITION)
        elif usr_string == 'p':
            serial.reset_input_buffer()
            serial.write(REPORT_POSITION)
            print("Encoder 1 position: " + serial.readall().decode('ascii'))
        elif usr_string == 'k':
            # Prompt the user for a new target speed
            new_Kp = input("Set Proportional Gain (Kp[%/RPM]): ").strip()
            # Check that the input was properly formatted
            try:
                float(new_Kp)
            except ValueError as e:
                print(e)
                continue

            serial.write(SET_KP)
            serial.write((new_Kp + '\n').encode('ascii'))
        elif usr_string == 'i':
            # Prompt the user for a new target speed
            new_Ki = input("Set Integrating Gain (Ki[%/Deg]): ").strip()
            # Check that the input was properly formatted
            try:
                float(new_Ki)
            except ValueError as e:
                print(e)
                continue

            serial.write(SET_KI)
            serial.write((new_Ki + '\n').encode('ascii'))
        elif usr_string == 'n':
            # Prompt the user for a new target speed
            new_speed = input("Set Motor 1 target speed (RPM): ").strip()
            # Check that the input was properly formatted
            try:
                float(new_speed)
            except ValueError as e:
                print(e)
                continue

            serial.write(SET_TARGET_SPEED)
            serial.write((new_speed + '\n').encode('ascii'))
        elif usr_string == 'r':
            collect_data(serial)

        else:
            print("Command '{}' not recognized.  Valid commands:".format(usr_string))
            print("     z - Zero the encoder 1 position\n"
                  "     p - Print out the encoder 1 position\n"
                  "     k - Set the Kp value\n"
                  "     i - Set the Ki value\n"
                  "     n - Set motor speed\n"
                  "     r - Run a predefined profile and plot performance\n")


def collect_data(serial):
    """
    @brief Command the motor to follow a profile and then collect data on how well it functions.
    @details Runs a ProfileFollower object based on the reference profile provided in class.  Sends new target speeds
             and positions to the nucleo whenever it can based on the time that has elapsed since starting the run.
             Collects data produced by the nucleo as it comes and then cleans all the data at the end (removes
             misreads).  Then generates a plot of the reference and measured response.
    @param serial A serial object used to communicate with the nucleo.
    """

    print("   Enter 's' to stop data collection.")
    raw_data = []

    # Start data collection
    pf = ProfileFollower(serial, "reference.xls")
    # pf = ProfileFollower(serial, "StepProfile.csv")
    pf.start()
    while pf.is_collecting:
        if pf.finished:
            break
        pf.write_target()
        if serial.in_waiting > 0:
            dp = serial.readline().decode('ascii').strip()
            raw_data.append(dp)

        if select.select([sys.stdin, ], [], [], 0.0)[0] and 's' == sys.stdin.read(1):
            pf.stop()
            break
    print("")

    raw_data = raw_data + serial.readall().decode('ascii').split('\n')

    # remove any data points that were misread
    data_points = clean_data(raw_data)
    # generate plot and files
    if _UI_DEBUG:
        print(data_points[:10], '...')  # print some data points
        print("Data Collection completed")

    file_time = datetime.now().strftime("%Y_%m_%d_%I-%M-%S_%p")
    # gen_csv("Profile_data{}.csv".format(file_time), data_points)
    gen_image("Profile_plot{}.png".format(file_time), data_points, pf)


def clean_data(raw_data):
    """
    @brief Removes any data points that were poorly formed due to serial connection issues.
    @param raw_data A list of strings collected from the Nucleo
    @returns A list of strings, all of the form "#.#, #.#, #.#"
    """
    regex = re.compile(r"^-?\d+.\d+, ?-?\d+.\d+, ?-?\d+.\d+$")
    return [line for line in raw_data if regex.match(line)]


def gen_csv(filename, data):
    """
    @brief Generate a .csv file from the given data.
    @details This function is unused in the final commit, however it has been left as it is still functional.
    @param filename The name of the file to write to
    @param data A list of strings of data to be added where each entry has the formation "#.#, #.#, #.#"
    """
    with open(filename, "w") as f:
        for line in data:
            f.write(line + '\n')


def gen_image(filename, data, profile):
    """
    @brief Generate a matplotlib plot from the given data.
    @details Generates two plots: velocity vs. time and position vs. time from the measured data and the reference
             profile using a matplotlib.pyplot.  The score (based on average variance) is reported at the top of the
             figure.
    @param filename The name of the file to write to
    @param data A list of data to be plotted where entries (time, velocity, position) are comma-separated floats
    @param profile A ProfileFollower object that contains the reference profile.
    """
    times = []
    positions = []
    velocities = []

    profile_times = []
    profile_positions = []
    profile_velocities = []
    score = 0

    # format data
    for line in data:
        tim, pos, vel = line.split(',')
        tim = float(tim)
        pos = float(pos)
        vel = float(vel)

        times.append(tim)
        positions.append(pos)
        velocities.append(vel)

        reference = profile.targets[int(tim * 1000)]
        ref_vel = float(reference["velocity"])
        ref_pos = float(reference["position"])

        profile_velocities.append(ref_vel)
        profile_positions.append(ref_pos)
        score += (ref_vel - vel) ** 2 + (ref_pos - pos) ** 2

    score /= len(data)
    print("Reference tracking score: {:.4f}".format(score))

    pyplot.figure()
    pyplot.suptitle("J = {:.4f}".format(score))
    # generate Velocity plot
    pyplot.subplot(2, 1, 1)
    pyplot.plot(times, velocities, color='red')
    pyplot.plot(times, profile_velocities, color='black')
    pyplot.title('Velocity')
    pyplot.ylabel('Velocity (RPM)')
    pyplot.savefig(filename)

    # generate Position plot
    pyplot.subplot(2, 1, 2)
    pyplot.plot(times, positions, color='blue')
    pyplot.plot(times, profile_positions, color='black')
    pyplot.xlabel('Time, t[s]')
    pyplot.ylabel('Position [deg]')
    pyplot.savefig(filename)


if __name__ == '__main__':
    main(sys.argv)

## @}
#
