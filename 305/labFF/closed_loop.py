## \addtogroup Lab0xFF
#  @{

""" @file closed_loop.py
    @brief Implements a closed loop PI controller for controlling a motor on the ME 405 project board
    @author Tyler Olson
    @date March 18, 2021
"""

from encoder import Encoder
from motor import Motor

import utime
import micropython

# for testing
from pyb import Pin, Timer

## Calculated Proportional Coefficient, Kp [%/RPM]
#
_CALCULATED_KP = 0.143

## Calculated Integrating Coefficient, Ki [%/Deg]
#
_CALCULATED_KI = .922

## Proportional Coefficient, Kp [%/RPM]
#
INITIAL_KP = _CALCULATED_KP

## Integrating Coefficient, Ki [%/Deg]
#
INITIAL_KI = _CALCULATED_KI

## Debug
#
_DEBUG = True


class ClosedLoop:
    """
    @brief A Closed Loop Controller object for controlling a motor/encoder pair attached to the ME405 board.
    @details This is a PI controller that takes in a desired position and velocity and outputs a PWM level from 0 to 100
             to drive the motor at.  Calculated coefficients are provided (called _CALCULATED_K) however these have been
             tuned to the INITIAL_K values based on step response and profile following.
    """

    def __init__(self, motor: Motor, encoder: Encoder):
        """
        @brief A constructor for the ClosedLoop controller object
        @param motor A Motor object to be controlled (must support PWM control)
        @param encoder An Encoder object that reports on the motor being controlled
        """
        self.motor = motor              ##< The motor that is being controlled. #
        self.encoder = encoder          ##< An encoder connected to the motor that reports position. #
        self.RUNNING = False            ##< Specifies if the controller is currently running or simply reporting. #
        self.Kp = INITIAL_KP            ##< Kp see /ref INITIAL_KP #
        self.Ki = INITIAL_KI            ##< Ki see /ref INITIAL_KI #

        # Set now as the start
        self.epoch = utime.ticks_us()   ##< Reference time [ms] for calculating delta T.  This is updated with velocity. #
        self.last_position = 0          ##< Reference position for calculating delta P.  This is also updated wth velocity. #
        self.encoder.set_position(0)
        self.velocity = 0               ##< The most recently measured velocity [RPM] #

        # Motor target Velocity (RPM) and position (DEG)
        self.target_speed = 0           ##< The desired motor velocity [RPM] #
        self.target_position = 0        ##< The desired motor position [Deg] #

        # static definition for update function
        self._update = self.update

    def update(self, target_speed=None, target_position=None):
        """
        @brief Update the controller.
        @details Updates the current velocity and changes the PWM level sent to the motor based on the velocity error
                 and position error.  Can adjust the target speed and position if provided.
        @param target_speed A new target speed (if specified)
        @param target_position A new target position (if specified)
        """
        if target_speed is None:
            target_speed = self.target_speed
        else:
            # if _DEBUG:
            #     print("Target Speed set to:", target_speed)
            self.target_speed = target_speed
        if target_position is None:
            target_position = self.target_position
        else:
            # if _DEBUG:
            #     print("Target Position set to:", target_position)
            self.target_position = target_position

        velocity = self.get_speed()
        if self.RUNNING:
            # proportional controller
            control_PWM = (target_speed - velocity) * self.Kp

            # PI Controller
            control_PWM += (target_position - self.encoder.get_position("degrees")) * self.Ki

            # Update PWM value to the motor
            self.motor.set_duty(control_PWM)

    def run(self, target_speed=None, target_position=None):
        """
        @brief Set the controller to automatically run at a target speed or position.
        @details Optionally updates the target speed or position.  If position is specified it should be updated at
                 regular intervals to avoid poor performance.  This stimulates a step response.
        @param target_speed The target speed for the new response
        @param target_position The target position for the new response
        """
        self.RUNNING = True
        if target_speed is not None:
            self.target_speed = target_speed
        if target_position is not None:
            self.target_position = target_position
        # self.update(target_speed, target_position)

    def stop(self):
        """
        @brief Stop the controller and wait to return until the system settles.
        @details This does not stop velocity or speed from being updated.
        """
        self.RUNNING = False
        self.motor.set_duty(0)

        # Wait for the system to settle (and then double check)
        while self.velocity != 0:
            pass
        self.update()
        while self.velocity != 0:
            pass

    def get_Kp(self):
        """
        @brief Get the controller's Kp value.
        """
        return self.Kp

    def set_Kp(self, Kp):
        """
        @brief Set the controller's Kp value.
        """
        self.Kp = Kp

    def get_Ki(self):
        """
        @brief Get the controller's Ki value.
        """
        return self.Ki

    def set_Ki(self, Ki):
        """
        @brief Set the controller's Ki value.
        """
        self.Ki = Ki

    def get_speed(self):
        """
        @brief Calculate and update the velocity of the motor based on the reference time and position.
        @return Returns the new velocity in RPM.
        """
        # calculate instantaneous velocity and position
        position = self.encoder.get_position(units="revolutions")
        time = utime.ticks_us()
        self.velocity = (position - self.last_position) / (utime.ticks_diff(time, self.epoch) / 1000000) * 60

        # set for next time
        self.epoch = time
        self.last_position = position

        # return the moving average
        return self.velocity


if __name__ == '__main__':
    # Encoder
    B6 = Pin("B6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    B7 = Pin("B7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    enc1 = Encoder(B6, B7, Timer(4, prescaler=0, period=0xFFFF))

    # Motor
    pin_nSLEEP = Pin("A15", mode=Pin.OUT_PP)
    pin_IN1 = Pin("B4", mode=Pin.OUT_PP)
    pin_IN2 = Pin("B5", mode=Pin.OUT_PP)
    timer = Timer(3, freq=20000)
    M1 = Motor(pin_nSLEEP, pin_IN1, pin_IN2, timer)
    M1.enable()

    controller = ClosedLoop(M1, enc1)

    while True:
        M1.set_duty(int(input("Set Motor 1 PWM: ")))
        epoch = utime.ticks_ms()
        while utime.ticks_diff(utime.ticks_ms(), epoch) < 5:
            controller.update()
            print("\rEncoder Position: {:9.2}[deg] Motor Speed: {:9.2}[RPM]".format(enc1.get_position(), controller.get_speed()), end="")


## @}
#
