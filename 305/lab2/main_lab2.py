## \addtogroup Lab0x02 Lab0x02
#  @brief Getting Started with Hardware
#  @details In this lab, a finite state system is used to change the wave function of the led 2 (green LED) on the
#           Nucleo board.
#           <a href="http://mechatronics.tyler-olson.com/ME305_lab2_video.mp4"><u>Here</u></a> is an example video
#           demonstrating how it works.
#  @image html lab2_State_Diagram.jpg width=30%
#  @image latex lab2_State_Diagram.jpg width=30%
#  @{

""" @file main_lab2.py
    @brief Contains all code for Lab 0x02 to change Nucleo LED waveform
    @author Tyler Olson
    @date February 3, 2021
"""

import math, utime
from pyb import Pin, ExtInt, Timer

## @brief create global LD2 object for LED
#
LD2 = Pin(Pin.cpu.A5, Pin.OUT_PP)


def main_lab2():
    """
    @brief Sets the Nucleo board LD2 LED to different waveforms when the blue button (B1) is pressed
    @details Alternates between three wave forms, triggered by the rising edge of B1.
             1. Square Wave
             2. Sine Wave
             3. Sawtooth Wave
             Successive button presses within 1 second are ignored.
    @image html lab2_Waveforms.jpg width=50%
    @image latex lab2_Waveforms.jpg width=50%
    """

    print("Welcome! Press the user button B1 (blue) on the Nucleo to cycle through LED patterns.")
    LD2.high()

    # set up a PWM timer @ 20kHz
    timer = Timer(2, freq=20000)
    timer.channel(1, Timer.PWM, pin=LD2)
    # start infinite loop
    while True:
        tick(timer, Button1.wave_fcn, utime.ticks_diff(utime.ticks_ms(), Button1.epoch))
        pass


def tick(timer: Timer, wave_fcn, time):
    timer.channel(1).pulse_width_percent(wave_fcn(time // 1000))


def cycle_waveform():
    {square_wave: sine_wave,
     sine_wave: saw}
    return utime.ticks_ms()


def square_wave(time: float):
    """
    @brief Defines a square wave with a period of 1s
    @param time The current time in seconds since the epoch
    @return Returns a float % duty cycle for a square wave based on the timer counter supplied.
    """

    if time % 1 < 0.5:
        return 100
    return 0


def sawtooth_wave(time: float):
    """
    @brief Defines a sawtooth wave with a period of 1s
    @param time The current time in seconds since the epoch
    @return Returns a float % duty cycle for a sawtooth wave based on the timer counter supplied.
    """

    return 100 * (time % 1)


def sine_wave(time: float):
    """
    @brief Defines a sine wave with a period of 10s
    @param time The current time in seconds since the epoch
    @return Returns a float % duty cycle for a sine wave based on the timer counter supplied.
    """

    return 100 * (math.sin(time * math.pi / 5) + 1)


class Button1(Pin):
    """
    @brief A wrapper for the Pin class that forces the button to deactivate for 1 second after being pressed.
    """

    @staticmethod
    def enable(trigger: Timer):
        """
        @brief re-enables the button after having been disabled by a timer
        @param trigger The timer that disabled the button
        """
        trigger.deinit()
        Button1.interrupt.enable()

    @staticmethod
    def _press(irq_src: int):
        """
        @brief Cycles the waveform when the button is pressed (limited to 1 press per second).
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        print("Button Pressed ( pin", irq_src, ")")

        # disable button and set a 1 second timer to re-enable
        Button1.interrupt.disable()
        Button1.timer.init(freq=1, callback=Button1.enable)

        # then cycle the waveform
        cycle_waveform()

    timer = Timer(2)
    interrupt = ExtInt(Pin.cpu.C13, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=_press)
    epoch = utime.ticks_ms()

    # default to square wave
    wave_fcn = square_wave

    def __init__(self):
        super()


if __name__ == '__main__':
    main_lab2()

## @}
#
