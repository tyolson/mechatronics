## \addtogroup Lab0x03 Lab0x03
#  @brief Simon Says Nucleo Game
#  @details In this lab, the user can play a rudimentary version of Simon Says with the Nucleo Board.
#           <a href="http://mechatronics.tyler-olson.com/ME305_lab3_video.mp4"><u>Here</u></a> is an example video
#           demonstrating how it works.
#  @image html lab3_State_Diagram.jpg width=30%
#  @image latex lab3_State_Diagram.jpg width=30%
#  @{

""" @file main.py
    @brief Contains the main state machine for Lab 0x03
    @author Tyler Olson
    @date February 17, 2021
"""

## @brief set a debug flag for extra messages and buffer space
#
_DEBUG = False
# Increase buffer for more helpful debugging
if _DEBUG:
    import micropython
    micropython.alloc_emergency_exception_buf(100)

import Button
import utime
from random import randint
from pyb import Pin

## @brief create global LD2 object for LED
#
LD2 = Pin(Pin.cpu.A5, Pin.OUT_PP)

## @brief Global specify starting length
#
STARTING_LENGTH = 3

## @brief Global specify max length
#
MAX_LENGTH = 10

## @brief Global definition of dot length in ms to be used by the LED (Dash length = 3 * Dot length)
#
DOT_LENGTH_MS = 200
DASH_LENGTH_MS = 3 * DOT_LENGTH_MS


class State:
    """
    @brief A class to hold the current state of the FSM as well as some helper functions for decoding
    """

    def __init__(self):
        """
        @brief Initialize the state variable (only needs to be called once)
        """
        ## @brief The current integer length of the sequence up to some MAX_LENGTH
        #
        self.length = STARTING_LENGTH
        ## @brief The complete MAX_LENGTH length sequence for this round
        #  @details The main sequence is stored as a bitfield with the least significant MAX_LENGTH bits holding the
        #           sequence with Dots (0s) and Dashes (1s).  It is randomly generated at the start of each round.
        self.sequence = self.generate_sequence(MAX_LENGTH)
        self.win = False
        self.lose = False
        self.success = False

    def try_sequence(self, user_sequence: int):
        """
        @brief Compares a formatted sequence to the current state sequence, sets condition flags appropriately.
        @details Sets State.win upon a win condition, State.lose if a lose condition, and State.success if the user
                 enters a successful subsequence.  Each of these flags are mutually exclusive. If the success flag is
                 set, the next subsequence (of length n + 1) will be displayed on the LED after a brief delay.
        @param user_sequence This is a sequence of the form LENGTH [31:24] SEQUENCE [23:0] where LENGTH is the length of
               the sequence to be tested and SEQUENCE is the bitfield sequence entered by the user.
        """
        # User sequence in first 24 bits
        sequence = user_sequence & 0x00FFFFFF
        # User sequence length in last 8 bits
        length = user_sequence >> 24

        if _DEBUG:
            print("Sequence:      {} = {}".format(self.sequence, self.decode_sequence(self.sequence >> (MAX_LENGTH - self.length), self.length)))
            print("User Sequence: {} = {}".format(sequence, self.decode_sequence(sequence, length)))
            print(self.sequence >> (MAX_LENGTH - self.length))
        if self.length == length and (self.sequence >> (MAX_LENGTH - self.length)) == sequence:
            if self.length == MAX_LENGTH:
                self.win = True
                print("You Won!")
            else:
                self.length += 1
                print("Success!, Next sequence")
                utime.sleep_ms(1000)
                self.light_sequence()
                self.success = True
        else:
            print("You failed :(")
            self.lose = True

    def light_sequence(self):
        """
        @brief Displays the current State.length entries of State.sequence on the LED.
        """
        for i in range(1, self.length + 1):
            if (self.sequence >> (MAX_LENGTH - i)) % 2 == 0:    # Dot
                light(DOT_LENGTH_MS)
            else:
                light(DASH_LENGTH_MS)

    @staticmethod
    def generate_sequence(length):
        """
        @brief Randomly generates a sequence of dots and dashes
        @param length The length of the sequence
        @return Returns a bitfield whose least significant bits contain the sequence of Dots (0s) and Dashes (1s)
        """
        seq = 0

        for i in range(length):
            code = randint(0, 1)
            seq = seq * 2 + code

        return seq

    @staticmethod
    def decode_sequence(sequence, length):
        """
        @brief Decodes the first length entries of a properly formatted binary sequence into a string of dots and dashes
        @param sequence The binary sequence to be decoded.
        @param length The number of bits to be decoded, starting from the LSB up to bit length
        @return Returns a string of space-separated dots and dashes starting with the LSB up to length entries
        """
        return " ".join(["-" if sequence & (1 << i) != 0 else "." for i in range(length)][::-1])

    def __repr__(self):
        """
        @brief A string representation of the state for debugging purposes
        @return "Sequence (length {length}: {sequence}"
        """
        return "Sequence (length {}): {}".format(self.length, self.decode_sequence(self.sequence, MAX_LENGTH))


def light(interval_ms):
    """
    @brief Light up the LED for some time, then waits for the minimum DOT_LENGTH_MS to mark a new character.
    @param interval_ms The length of time to hold the LED on in ms
    """
    LD2.high()
    utime.sleep_ms(interval_ms)
    LD2.low()
    utime.sleep_ms(DOT_LENGTH_MS)


def main():
    """
    @brief Runs the Simon Says game.
    @details First display a welcome message and instructions to the user.  Then display a sequence of length 3.  If the
             user successfully responds with the matching sequence of button presses, increase the the sequence length.
             Once the sequence has reached MAX_LENGTH, declare the user a winner and prompt to play again.  If the user
             is ever unsuccessful, declare them a loser and prompt to play again.  If the user does not want to continue
             the display their Win/Loss record.
    """

    state = State()
    if _DEBUG:
        print(state)
    print("Welcome to Simon says Morse Code!\n"
          " * Simply press the blue button in the same sequence of dots (short presses) and dashes (long presses) as "
          "appear on the LED.\n"
          " * Sequences will increase in length from 3 to 10.  If you match the last sequence perfectly, you win!  "
          "Wins and losses will be tracked so you can brag to your buddies!")
    try:
        input("Press the enter key to start.")
    except KeyboardInterrupt:
        print()
    print("The round is starting!")
    utime.sleep_ms(2000)
    state.light_sequence()

    B1 = Button.Button(state.try_sequence)
    wins = 0
    losses = 0

    # start infinite loop
    while True:
        if state.success:
            B1.enable()
            state.success = False
        if not state.win and not state.lose:
            continue

        if state.win:
            wins += 1
        else:   # losing condition
            losses += 1

        try:
            input("Press the enter key to play again (or CTRL-C to quit)")
        except KeyboardInterrupt:
            print()
            break
        except EOFError as e:
            print()
            break

        # reset state
        state.lose = False
        state.win = False
        state.length = STARTING_LENGTH
        state.sequence = state.generate_sequence(MAX_LENGTH)
        B1.enable()
        print("The round is starting!")
        utime.sleep_ms(2000)
        state.light_sequence()

    print("Win / Loss: {}/{}".format(wins, losses))


if __name__ == '__main__':
    main()

## @}
#
