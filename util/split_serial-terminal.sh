#!/bin/bash

# nucleo_connect
screen -S nucleo -c ~/mech/util/screen_terminal-serial

# nucleo_disconnect
screen -X -S nucleo quit
