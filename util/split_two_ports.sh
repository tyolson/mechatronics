#!/bin/bash

# nucleo_connect
screen -S nucleo -c screen_two_ports

# nucleo_disconnect
screen -X -S nucleo quit
