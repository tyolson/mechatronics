#!/bin/bash
cd ~/mech/405

# add py_filter to PATH so doxygen can use it
export PATH="$HOME/anaconda3/envs/mech/bin:$PATH"

doxygen doxyfile
firefox html/index.html
