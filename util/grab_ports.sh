#!/bin/bash
if [ $(ls /dev/serial/by-id | wc -l) -ne 0 ]; then
    echo "Nucleo detected."
else
    return
fi

if [ $(ls /dev/serial/by-id | wc -l) -eq 1 ]; then
    export ST_LINK=/dev/serial/by-id/$(ls /dev/serial/by-id)
    export AMPY_PORT=$ST_LINK
    echo ST_LINK: $ST_LINK
else
    export USB_VCP=/dev/serial/by-id/$(ls /dev/serial/by-id | head -1)
    export ST_LINK=/dev/serial/by-id/$(ls /dev/serial/by-id | tail -1)
    export AMPY_PORT=$USB_VCP
    echo ST_LINK: $ST_LINK
    echo USB_VCP: $USB_VCP
fi
