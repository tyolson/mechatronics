#!/bin/bash

# Connect nucleo to the USB Virtual Comm port
screen -S nucleo $ST_LINK 115200

# nucleo_disconnect
screen -X -S nucleo quit
