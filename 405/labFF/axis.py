## \addtogroup Lab0xFF
#  @{

""" @file axis.py
    @brief Defines a container class to hold state for a single axis on the ME 405 platform.
    @author Tyler Olson
    @date June 8, 2021
"""
from controller import Controller
from drivers.Encoder import Encoder

## Distance from the platform pivot to the U-joint [m]
PLATFORM_PIVOT_OFFSET = 0.110
## Length of the lever arm [m]
LEVER_ARM_RADIUS = 0.060


class Profile:
    """
    @brief A container class for state variables of a given axis.
    """

    def __init__(self, axis: str):
        """
        @brief Initialize a Profile object with all zero values and a named axis.
        @param axis The name of the axis held here.
        """

        ## The state variable representing linear position [m] of the ball in this direction
        self.pos = 0
        ## The state variable representing linear velocity [m/s] of the ball in this direction
        self.vel = 0
        ## The state variable representing angular position [rad] of the platform in the orthogonal direction
        self.theta = 0
        ## The state variable representing angular velocity [rad/s] of the platform in the orthogonal direction
        self.theta_dot = 0
        ## The current torque being applied to the motor [N-m]
        self.torque = 0
        ## The name of the cardinal direction.
        #  Parallel to self.pos and self.vel vectors, perpendicular to self.theta and self.theta_dot vectors)
        self.axis = axis

    def reset(self):
        """
        @brief Reset the profile to zero.
        """
        self.pos = 0
        self.vel = 0
        self.theta = 0
        self.theta_dot = 0
        self.torque = 0


class Axis:
    """
    @brief _brief_description
    @details _detailed_description
    """

    def __init__(self, axis: str, controller: Controller, encoder: Encoder):
        """
        @brief Create an Axis object.
        @param axis The name of the axis
        @param controller A Controller object that acts on this Axis
        @param encoder An Encoder object that measures the angle controlling this axis.
        """
        self.controller = controller
        self.encoder = encoder
        self.profile = Profile(axis)
        self.last_times = {"pos": 0.0, "theta": 0.0}

    # TODO: add a decorator to wrap time updating
    def update_pos(self, now: float, new_pos: float):
        """
        @brief Update the position and velocity of this axis.
        @param now The current time measured in seconds.
        @param new_pos The new position in meters
        """
        self.profile.vel = (new_pos - self.profile.pos) / (now - self.last_times["pos"])
        self.profile.pos = new_pos
        self.last_times["pos"] = now

    def update_theta(self, now):
        """
        @brief Update the angle and angular velocity of this axis.
        @param now The current time measured in seconds.
        """
        new_theta = self.encoder.get_position("radians") * LEVER_ARM_RADIUS / PLATFORM_PIVOT_OFFSET
        self.profile.theta_dot = (new_theta - self.profile.theta) / (now - self.last_times["theta"])
        self.profile.theta = new_theta
        self.last_times["theta"] = now

    def reset(self):
        """
        @brief Reset the axis.
        """
        self.profile.reset()
        self.encoder.set_position(0)
        self.controller.torque = 0

## @}
#
