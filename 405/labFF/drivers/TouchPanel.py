## \addtogroup Lab0xFF
#  @{

""" @file TouchPanel.py
    @brief Driver for the Adafruit 8.75" touch panel
    @details This driver is intended to be used with the Adafruit 8.75" Resistive Touch panel as mounted on the ME405
             Lab0xFF platform.  Default settings are based on calibration data taken May 25, 2021.  Calibrations were
             taken by first laying out a grid on top of the touch panel with known real locations, then finding the
             average voltage (in adc counts) along each major line as shown below:
             @image html touchPanelCalibration_grid.jpg width=40%
             The default SLOPE and INTERCEPT arrays were calibrated using these plots:
                 <div style="display:flex">
                 @image html touchPanelCalibration_y-scaling.png width=100%
                 @image html touchPanelCalibration_x-scaling.png width=100%
                 </div>
    @author Tyler Olson
    @date June 5, 2021
"""

from pyb import Pin, ADC
import utime

## @brief Offsets to center of resistive touch panel
#  @details These offsets do not necessarily coincide with the geometric center of every platform.
#
INTERCEPT = [2008.5, 2120.3]

## Slope (adc counts per mm) for each coordinate
SLOPE = [20.364, -30.05]

## Threshold for digital 'LOW' in adc counts
LOW_THRESHOLD = 100


class TouchPanel:
    """
    @brief A class for collecting information from the touch panel over I2C
    @details This class can be used to get X and Y positions as well as scan for contact.
    """

    def __init__(self, x_m, x_p, y_m, y_p, width=None, length=None, origin=INTERCEPT):
        """
        @brief Initialize a TouchPanel driver object.
        @details This object expects the pins connected to +/-x and +/-y.  Optionally a width, length, and origin can be
                 specified in order use this class with a different sized touch panel than provided on the ME 405 test
                 bench, or to tune where the exact center is.  Note that the physical dimensions are always assumed to
                 be 176mm x 100mm.
        @param x_m A pyb.Pin object connected to x-.  This object must be connected to an analog adc.
        @param x_p A pyb.Pin object connected to x+.  This object must be connected to an analog adc.
        @param y_m A pyb.Pin object connected to y-.
        @param y_p A pyb.Pin object connected to y+.  This object must be connected to an analog adc.
        @param width Optionally set the width by measuring the difference in adc counts across the touch panel.
        @param length Optionally set the length by measuring the difference in adc counts across the touch panel.
        @param origin Optionally set the geometric origin by providing an [x, y] coordinate offset (as measured in adc
                      counts).
        """
        ## x- pin
        self.x_m = x_m
        ## x+ pin
        self.x_p = x_p
        ## y- pin
        self.y_m = y_m
        ## y+ pin
        self.y_p = y_p

        ## Slopes used for linear calculation of position
        self.slope = SLOPE
        if width:
            self.slope[1] = width / 100
        if length:
            self.slope[0] = length / 176
        ## Origin used for linear calculation of position
        self.origin = origin

    def get_x(self):
        """
        @brief Get the x-coordinate of the current touch.
        @details Sets x- pin to low, x+ pin to high, floats the y+ pin, and takes an analog measurement of the y- pin.
                 Pins are left in this mode upon exit.
        @return Returns the x-coordinate in mm of the current touch.  If the panel is not being touched then the value
                returned is unspecified.
        """
        self.x_m.init(Pin.OUT_PP)
        self.x_m.low()
        self.x_p.init(Pin.OUT_PP)
        self.x_p.high()
        self.y_p.init(Pin.IN)
        adc = ADC(self.y_m)

        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        return (adc.read() - self.origin[0]) / self.slope[0]

    def get_y(self):
        """
        @brief Get the y-coordinate of the current touch.
        @details Sets y- pin to low, y+ pin to high, floats the x- pin, and takes an analog measurement of the x+ pin.
                 Pins are left in this mode upon exit.
        @return Returns the y-coordinate in mm of the current touch.  If the panel is not being touched then the value
                returned is unspecified.
        """
        self.y_m.init(Pin.OUT_PP)
        self.y_m.low()
        self.y_p.init(Pin.OUT_PP)
        self.y_p.high()
        self.x_m.init(Pin.IN)
        adc = ADC(self.x_p)

        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        return (adc.read() - self.origin[1]) / self.slope[1]

    def has_contact(self):
        """
        @brief Detect if the touch panel is currently being touched
        @details Sets y- pin to high, x- pin to low, floats the y- and pin, and takes an analog measurement of the x+
                 pin.  Pins are left in this mode upon exit.  Checks to see if the measured voltage is higher than
                 LOW_THRESHOLD.
        @return Returns the true if the panel is being touched, otherwise false.
        """
        self.y_p.init(Pin.OUT_PP)
        self.y_p.high()
        self.y_m.init(Pin.IN)
        self.x_m.init(Pin.OUT_PP)
        self.x_m.low()
        adc = ADC(self.x_p)
        
        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        return adc.read() > LOW_THRESHOLD

    def scan(self):
        """
        @brief Scan the touch panel to detect for contact (z coordinate) and x and y position.
        @details Scans x, z, then y coordinates, taking advantage of pin mode settings for a faster read time than
                 scanning each individually. If the panel is not being touched then the values for x and y position
                 returned are meaningless.
                 Results of timing tests using test_TouchPanel-timing.py :
                 <table>
                 <tr>
                    <td><strong>method</td>
                    <td>average time (µs)</strong></td>
                 </tr>
                 <tr>
                    <td>get_x</td>
                    <td>330</td>
                 </tr>
                 <tr>
                    <td>get_y</td>
                    <td>330</td>
                 </tr>
                 <tr>
                    <td>has_contact</td>
                    <td>295</td>
                 </tr>
                 <tr>
                    <td><i>scan</td>
                    <td>660</i></td>
                 </tr>
                 </table>
        @return Returns a tuple of [x, y, z] where x and y are coordinates in mm and z is a boolean value dictating if
                the panel is currently being touched.
        """
        # Step 1: read x coordinate
        self.y_m.init(Pin.IN)
        self.x_p.init(Pin.OUT_PP)
        self.x_p.high()
        self.x_m.init(Pin.OUT_PP)
        self.x_m.low()
        adc = ADC(self.y_p)
        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        x = (adc.read() - self.origin[0]) / self.slope[0]

        # Step 2: read z (check if touching)
        self.y_p.init(Pin.OUT_PP)
        self.y_p.high()
        adc = ADC(self.x_p)
        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        z = adc.read() > LOW_THRESHOLD

        # Step 2: read y coordinate
        self.y_m.init(Pin.OUT_PP)
        self.y_m.low()
        self.x_m.init(Pin.IN)
        # wait for the touch panel to settle (round up to 4us)
        utime.sleep_us(4)
        y = (adc.read() - self.origin[1]) / self.slope[1]

        return x, y, z

## @}
#


if __name__ == '__main__':
    panel = TouchPanel(Pin.cpu.A7, Pin.cpu.A6, Pin.cpu.A0, Pin.cpu.A1)
    print("Testing Touch Panel")

    while True:
        x_coord, y_coord, contacted = panel.scan()
        if contacted:
            print("[  yes  ] x: {:6.3f}(mm) y: {:6.3f}(mm)".format(x_coord, y_coord), end="\r")
        else:
            print("[  no  ]                               ", end="\r")
