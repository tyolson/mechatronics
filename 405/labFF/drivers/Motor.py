## \addtogroup Lab0xFF
#  @{

""" @file Motor.py
    @brief Drivers for bushed DC motors and Texas Instruments DRV8847 motor driver.
    @details This driver class based on the motor.py class developed in ME 305, with added overcurrent protection.
             Motor objects can be created with input and output pins and a timer to control PWM.  Only 1 MotorDriver
             object can exist at a time, which is given control of 2 Motors.
    @author Tyler Olson
    @date June 5, 2021
"""

from pyb import Pin, Timer, ExtInt
import micropython

## Set a limit for the board for maximum number of channels
#
MAX_CHANNEL_NUM = 4


class Motor:
    """
    @brief This class implements a motor driver for the ME405 board.
    """

    def __init__(self, pin_ch1, pin_ch2, timer):
        """
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param pin_ch1 A pyb.Pin object to use as the input to half bridge 1.
        @param pin_ch2 A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on either pin.
        """
        # Get the next two available channels
        i = 1
        while timer.channel(i) is not None:
            if i > MAX_CHANNEL_NUM - 1:
                raise ValueError("Not enough free channels on this timer")
            i += 1

        # Set PWM channels, default to 0%
        ## Forward direction PWM channel
        self.ch1 = timer.channel(i, Timer.PWM, pin=pin_ch1, pulse_width_percent=0)
        ## Reverse direction PWM channel
        self.ch2 = timer.channel(i+1, Timer.PWM, pin=pin_ch2, pulse_width_percent=0)

    def disable(self):
        """
        @brief Disable the motor and set its speed to zero.
        @details Caution: this method will disable any motors attached to its enable pin.
        """
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)

    def set_duty(self, duty):
        """
        @brief Set the motor duty cycle.
        @details This method sets the duty cycle to be sent to the motor to the given level. Positive values cause
                 effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        """
        if duty > 0:
            self.ch1.pulse_width_percent(duty)
            self.ch2.pulse_width_percent(0)
        else:
            self.ch2.pulse_width_percent(duty * -1)
            self.ch1.pulse_width_percent(0)


class MotorDriver:
    """
    @brief A driver class for interacting with two motors while also protecting against over-current.
    @details This motor driver object mirrors the Texas Instruments DRV8847 chip as configured on the ME 405 board.
             It allows the user to drive two Motor objects independently, with a common enable pin and a common fault
             pin.  Only one MotorDriver object should exist at a time.
    """

    def __init__(self,
                 pin_nSLEEP=Pin("A15", mode=Pin.OUT_PP),
                 pin_nFAULT=Pin("B2", mode=Pin.OUT_PP),
                 pin_IN1=Pin("B4", mode=Pin.OUT_PP),
                 pin_IN2=Pin("B5", mode=Pin.OUT_PP),
                 pin_IN3=Pin("B0", mode=Pin.OUT_PP),
                 pin_IN4=Pin("B1", mode=Pin.OUT_PP),
                 timer=Timer(3, freq=20000)):
        """
        @brief Initialize a motor driver instance.
        @details Create a motor driver object mirroring the Texas Instruments DRV8847 chip.  If no arguments are passed,
                 the driver will assume the pins are as specified in the lab spec.  While it is never necessary to use
                 both channels of this driver, it assumes that two are connected.
        @param pin_nSLEEP A pyb.Pin object to use as the enable pin.
        @param pin_nFAULT A pyb.Pin object to use as the over-current interrupt pin.
        @param pin_IN1 A pyb.Pin object to use as the input to half bridge 1.
        @param pin_IN2 A pyb.Pin object to use as the input to half bridge 2.
        @param pin_IN3 A pyb.Pin object to use as the input to half bridge 3.
        @param pin_IN4 A pyb.Pin object to use as the input to half bridge 4.
        """
        # start in a disabled state
        ## Sleep Pin (active high)
        self.nSLEEP = pin_nSLEEP
        self.nSLEEP.low()
        ## Enabled state of the MotorDriver
        self.enabled = False
        ## A boolean flag indicating if the MotorDriver was disabled due to an over-current fault
        self.FAULTED = False

        # Set up overcurrent fault detection
        ## Fault Pin (active high) for detecting overcurrent
        self.nFAULT = pin_nFAULT
        ## Interrupt to read Fault Pin
        self.fault_int = ExtInt(self.nFAULT, ExtInt.IRQ_FALLING, Pin.PULL_NONE, self.fault_callback)
        self.fault_int.disable()

        ## Motor object on Channel 1
        self.ch1 = Motor(pin_IN1, pin_IN2, timer)
        ## Motor object on Channel 2
        self.ch2 = Motor(pin_IN3, pin_IN4, timer)

        self._reset = lambda _: self.reset()

    def enable(self):
        """
        @brief Enable the motor driver.
        @details Caution: this method will enable all motors attached to its enable pin.
        """
        self.nSLEEP.high()
        self.nFAULT.init(Pin.OUT_PP)
        self.nFAULT.high()
        self.fault_int.enable()
        self.enabled = True
        self.FAULTED = False

    def disable(self):
        """
        @brief Disable the motor driver and set motor speeds to zero.
        @details Caution: this method will disable both motors attached to its enable pin.
        """
        self.fault_int.disable()
        self.ch1.disable()
        self.ch2.disable()
        self.enabled = False
        self.nSLEEP.low()

    def fault_callback(self, _):
        """
        @brief Disable the motor driver when an overcurrent fault is detected.
        """
        self.disable()
        self.FAULTED = True
        #micropython.schedule(self._reset, None)

    def reset(self):
        """
        @brief Reset the motor driver after having been disabled by an overcurrent fault.
        @details If the user elects not to resume operation, a KeyboardInterrupt is raised.
        """
        answer = input("A motor overcurrent event was detected.  Proceed [Y/n]? ").lower()
        if answer == "n" or answer == "no":
            raise KeyboardInterrupt
        else:
            self.enable()
            print("")

## @}
#


if __name__ == '__main__':
    driver = MotorDriver()
    M1 = driver.ch1
    M2 = driver.ch2
    driver.enable()

    # Prompt the user for a new duty cycle
    while True:
        if driver.enabled:
            M1.set_duty(int(input("Set Motor 1 PWM: ")))
            M2.set_duty(int(input("Set Motor 2 PWM: ")))
        else:
            driver.reset()
