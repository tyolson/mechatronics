## \addtogroup Lab0xFF
#  @{

""" @file controller.py
    @brief A controller object that updates a single motor with full-state feedback.
    @author Tyler Olson
    @date _date, 2021
"""

from drivers.Motor import Motor

## Controller gains as calculated in \ref pg_hw5 .
K = [-1.9230949511259, -0.817604962125815, -0.617011551699976, -0.07079210481062]

## Constant multiple to convert torque from [N-m] to [PWM%]
#  <a href="ME_405_Coefficient_calcs.pdf" target="_blank"><b>Click Here</b></a> for calculations.
TORQUE_CONVERSION_FACTOR = 800.72


class Controller:
    """
    @brief A controller object operating with full-state feedback.
    """
    def __init__(self, motor: Motor):
        """
        @brief Initialize a Controller object and give it a motor to control.
        @param motor A Motor to be controlled.
        """
        self.motor = motor
        self.torque = 0

    def update(self, profile):
        """
        @brief Update the torque applied to the motor based on the measured system.
        @param profile A Profile object that contains the current state.
        """
        self.torque = K[0] * profile.pos + K[1] * profile.theta + K[2] * profile.vel + K[3] * profile.theta_dot
        profile.torque = self.torque
        self.motor.set_duty(self.torque * TORQUE_CONVERSION_FACTOR)

## @}
#
