## \addtogroup Lab0xFF Lab0xFF
#  @{

""" @file main.py
    @brief Entry point to the ball balancing test bench activity.
    @author Tyler Olson
    @date June 9, 2021
"""

import utime
import ujson
import pyb
import micropython

from pyb import Pin, Timer
from drivers.Encoder import Encoder
from drivers.Motor import MotorDriver
from drivers.TouchPanel import TouchPanel
from axis import Axis
from controller import Controller

from serial_codes import *

_DEBUG = True
if _DEBUG:
    micropython.alloc_emergency_exception_buf(100)


class State:
    """
    @brief The current state of the board.
    """

    ## Indicates if the nucleo is collecting data or not
    COLLECTING = False

    ## The time that controller was initiated
    _epoch = utime.ticks_us()

    ## Touch panel driver
    touch_panel = TouchPanel(Pin.cpu.A7, Pin.cpu.A6, Pin.cpu.A0, Pin.cpu.A1)

    ## Motor driver
    driver = MotorDriver()

    ## Two axies of the system, each operating independently
    axies = [Axis("x",
                  Controller(driver.ch2),
                  Encoder(Pin("C6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8),
                          Pin("C7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8),
                          Timer(8, prescaler=0, period=0xFFFF))),
             Axis("y",
                  Controller(driver.ch1),
                  Encoder(Pin("B6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4),
                          Pin("B7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4),
                          Timer(4, prescaler=0, period=0xFFFF)))]

    ## Current system time [s]
    time = 0.0

    @staticmethod
    def capture():
        """
        @brief Measure all state geometry at this instant and update the current state.
        """

        # get angles
        for axis in State.axies:
            axis.encoder.update()

        # assume time right in the middle of data collection
        State.time = now(State._epoch, "us") / 1000000

        # if found update the ball position, otherwise assume it hasn't changed
        x, y, has_contact = State.touch_panel.scan()
        if has_contact:
            State.axies[0].update_pos(State.time, x / 1000)
            State.axies[1].update_pos(State.time, y / 1000)

        # update the state
        for axis in State.axies:
            axis.update_theta(State.time)

    @staticmethod
    def update():
        """
        @brief Update the controllers for the current state.
        """
        # capture current profile and kinematics
        State.capture()

        # update the controller
        for axis in State.axies:
            axis.controller.update(axis.profile)


    @staticmethod
    def enqueue_data(stream):
        """
        @brief Combine the current state into a JSON string and send it to a file stream.
        @param stream A file object to write the data point to.
        """

        if not State.COLLECTING:
            return

        data = {"time": State.time,
                "x_profile": State.axies[0].profile.__dict__,
                "y_profile": State.axies[1].profile.__dict__}

        ujson.dump(data, stream)
        stream.write("\n")

    @staticmethod
    def collect():
        """
        @brief Reset the state and enable data collection.
        """
        State.COLLECTING = True
        State._epoch = utime.ticks_us()
        State.time = 0.0


def now(epoch, units):
    """
    @brief Get the difference between the epoch and now.
    @param epoch The starting time from which to measure the difference.
    @param units The units that the starting time is represented in.
    @return The time passed since the epoch in the units provided.
    """

    time_funcs = {"ms": utime.ticks_ms,
                  "us": utime.ticks_us}

    return utime.ticks_diff(time_funcs[units](), epoch)


def main():
    """
    @brief This is the entry point to the system control.
    @details This function is responsible for running the system including updating the controller, collecting and
             sending data, and receiving user input.
    """

    # Set Up
    # Stop sending debug data to UART
    pyb.repl_uart(None)
    serial = pyb.UART(2)

    # Two inner functions to handle interrupts
    def _update(_):
        State.update()
        State.enqueue_data(serial)

    def _schedule(_):
        try:
            micropython.schedule(_update, None)
        except RuntimeError:
            pass

    # A buffer for executing multiple commands at once if they exist
    command_buffer = ""

    # Start 30Hz timer to update the state and controller
    timer = Timer(1, freq=30)

    # Main Loop
    while True:
        # check for user input (Skip if none)
        if serial.any() == 0:
            continue
        if _DEBUG:
            print("")

        # Poll the serial line execute all commands up to a newline
        buffer = command_buffer + serial.read().decode('ascii')
        command_buffer = buffer[buffer.rfind('\n') + 1:]
        commands = buffer[:buffer.rfind('\n') + 1]

        # First check if data collection must be stopped
        if State.COLLECTING and check_for_command(STOP_DATA_COLLECTION, commands):
            State.COLLECTING = False
            if _DEBUG:
                print("Data collection completed")

        # Check if data needs to be collected
        if check_for_command(START_DATA_COLLECTION, commands):
            State.collect()
            if _DEBUG:
                print("Data collection started")

        # Other Misc Commands
        if check_for_command(CALIBRATE, commands):
            # Zero encoder 1 position
            timer.callback(None)
            State.driver.disable()
            if _DEBUG:
                print("Activating calibration mode.")

        # Start the controller and state
        if check_for_command(RUN, commands):
            # reset the profiles
            for axis in State.axies:
                axis.reset()
            timer.callback(_schedule)
            State.driver.enable()
            if _DEBUG:
                print("(Re)starting controller.")

        # terminate the program gracefully
        if check_for_command(HALT, commands):
            State.driver.disable()
            break

        # report to the frontend if a fault has occurred
        if State.driver.FAULTED:
            serial.write("FAULT")

    print("Goodbye!")


def check_for_command(command, buffer):
    """
    @brief Checks if a certain command was read on the serial input line.
    @param command The command to search for
    @param buffer The serial buffer, as a string
    """
    return command.decode('ascii') in buffer


if __name__ == '__main__':
    main()

## @}
#
