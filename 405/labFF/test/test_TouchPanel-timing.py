## \addtogroup Lab0xFF
#  @{

""" @file test_TouchPanel-timing.py
    @brief Compare the timing of individually scanning each coordinate vs scanning all at once.
    @author Tyler Olson
    @date June 5, 2021
"""

import utime

from pyb import Pin
from drivers.TouchPanel import TouchPanel


def main():
    """
    @brief Compare the timing of individually scanning each coordinate vs scanning all at once.
    """
    panel = TouchPanel(Pin.cpu.A7, Pin.cpu.A6, Pin.cpu.A0, Pin.cpu.A1)
    print("Testing Touch Panel timing")
    # total time in s
    total_time = 0

    # Scan the panel 1000 times
    for _ in range(1000):
        before_time = utime.ticks_ms()
        panel.scan()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000

    print("Total time for 1000 scans: {:3.3f}s".format(total_time))
    total_time = 0

    # Read the x-coordinate 1000 times
    for _ in range(1000):
        before_time = utime.ticks_ms()
        panel.get_x()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000

    print("Total time for 1000 get_xs: {:3.3f}s".format(total_time))
    total_time = 0

    # Read the y-coordinate 1000 times
    for _ in range(1000):
        before_time = utime.ticks_ms()
        panel.get_y()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000

    print("Total time for 1000 get_ys: {:3.3f}s".format(total_time))
    total_time = 0

    # Test cor contact 1000 times
    for _ in range(1000):
        before_time = utime.ticks_ms()
        panel.has_contact()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000

    print("Total time for 1000 has_contacts: {:3.3f}s".format(total_time))
    total_time = 0


if __name__ == '__main__':
    main()

## @}
#
