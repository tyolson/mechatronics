## \addtogroup Lab0xFF
#  @{

""" @file test_Encoder-timing.py
    @brief Calculate the average time of encoder updating
    @author Tyler Olson
    @date June 5, 2021
"""

import utime

from pyb import Pin, Timer
from drivers.Encoder import Encoder

## @}
#


class MockController:

    def __init__(self, encoder):
        self.velocity = 0
        self.encoder = encoder
        self.epoch = utime.ticks_us()
        self.last_position = 0

    def get_speed(self):
        """
        @brief Calculate and update the velocity of the motor based on the reference time and position.
        @return Returns the new velocity in RPM.
        """
        # calculate instantaneous velocity and position
        position = self.encoder.get_position(units="revolutions")
        time = utime.ticks_us()
        self.velocity = (position - self.last_position) / (utime.ticks_diff(time, self.epoch) / 1000000) * 60

        # set for next time
        self.epoch = time
        self.last_position = position

        # return the moving average
        return self.velocity


def main():
    """
    @brief Compare the timing of individually scanning each coordinate vs scanning all at once.
    """
    B6 = Pin("B6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    B7 = Pin("B7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF2_TIM4)
    C6 = Pin("C6", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8)
    C7 = Pin("C7", mode=Pin.AF_PP, pull=Pin.PULL_NONE, af=Pin.AF3_TIM8)
    enc1 = Encoder(B6, B7, Timer(4, prescaler=0, period=0xFFFF))
    enc2 = Encoder(C6, C7, Timer(8, prescaler=0, period=0xFFFF))

    print("Testing Encoder update timing")
    # total time in s
    total_time = 0

    # Read the x-coordinate 100,000 times
    for i in range(100000):
        before_time = utime.ticks_ms()
        enc1.update()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000
        if i % 100 == 0:
            print("\rEncoder 1: {:9.2} Encoder 2: {:9.2}".format(enc1.get_position(), enc2.get_position()), end="")

    print("\nTotal time for 100,000 enc1 updates: {:3.3f}s".format(total_time))
    total_time = 0

    # Read the y-coordinate 100,000 times
    for i in range(100000):
        before_time = utime.ticks_ms()
        enc2.update()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000
        if i % 100 == 0:
            print("\rEncoder 1: {:9.2} Encoder 2: {:9.2}".format(enc1.get_position(), enc2.get_position()), end="")

    print("\nTotal time for 100,000 enc2 updates: {:3.3f}s".format(total_time))
    total_time = 0

    mc = MockController(enc1)

    # Calculate speed 100,000 times
    for i in range(100000):
        before_time = utime.ticks_ms()
        mc.get_speed()
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000

    print("\nTotal time for 100,000 enc2 updates: {:3.3f}s".format(total_time))


if __name__ == '__main__':
    main()
