## \addtogroup Lab0xFF
#  @{

""" @file test_Main-timing.py
    @brief Calculate the average time of main loop.
    @author Tyler Olson
    @date June 8, 2021
"""

import pyb
import utime

from main import State


def test_timing():
    """
    @brief Test how long an update cycle takes
    """

    # Set Up
    # Stop sending debug data to UART
    pyb.repl_uart(None)
    serial = pyb.UART(2)

    State.COLLECTING = True
    State.driver.enable()

    # Update the state 1,000 times
    total_time = 0

    for i in range(1000):
        before_time = utime.ticks_ms()
        State.update()
        State.enqueue_data(serial)
        total_time += (utime.ticks_diff(utime.ticks_ms(), before_time)) / 1000
        if i % 100 == 0:
            State.enqueue_data(serial)

    print("\nTotal time for 1,000 update cycles: {:3.3f}s".format(total_time))


if __name__ == '__main__':
    test_timing()

## @}
#
