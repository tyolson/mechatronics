## \addtogroup Lab0xFF
#  @{

""" @file UI_front.py
    @brief Contains the front-end User Interface for interacting with the ME 405 board
    @details This frontend is currently only supported on Linux environments (64-bit Ubuntu 20.04.1).  It is based on
             the UI protocol developed for the ME 305 final project.
    @author Tyler Olson
    @date June 9, 2021
"""
import os
import sys
import select
import json
from datetime import datetime

from serial_codes import *

from serial import Serial
from matplotlib import pyplot
import numpy as np


_UI_DEBUG = True


def main(args):
    """
    @brief Start and run the UI frontend.
    @param args Accepts a commandline argument vector of 2 arguments:
                    the first is the program name,
                    the second is the name of the serial port.
    @details Waits for the user to enter any valid command.  When data collection is complete,
             generates both an image (using matplotlib) and a .CSV file of data (timestamps and data readings).
             Image and Data files are timestamped.
             c - Halt the system so the user can re-calibrate encoder positions with a level (default state).
             r - Activate the controller to start running.
             s - Stop the controller and process data.
             q - Halt the system and exit.

             If there is a motor fault, the user will be prompted to either clear the fault and resume normal operation
             or to terminate the program.  If the program is terminated, data collection is terminated.
    """

    # Define Serial port as ST-LINK
    print(args[0])
    serial = Serial(port=args[1], baudrate=115273, timeout=1)

    # buffer for raw data
    raw_data = []

    # Start infinite Loop.
    while True:
        usr_string = input("Enter a command [crsq]: ")

        # A command was found, handle it.
        if usr_string == 'c':
            serial.write(CALIBRATE)
        elif usr_string == 'r':
            serial.reset_input_buffer()
            serial.write(RUN)
            serial.reset_input_buffer()
            serial.write(START_DATA_COLLECTION)
            print("Press 's' to stop data collection")

            # wait for the user to press s
            COLLECTING = True
            while COLLECTING:
                if select.select([sys.stdin, ], [], [], 0.0)[0] and 's' == sys.stdin.read(1):
                    COLLECTING = False
                try:
                    collect_data(serial, raw_data)
                except InterruptedError as e:
                    answer = input("A motor fault has been detected.  Resume operation [Y/n]? ")
                    if "n" in answer.lower():
                        serial.write(HALT)
                        print("Terminating program")
                        break
                    serial.write(RUN)
            print("Finished collecting")

            load_anim = ["|", "/", "*", "\\"]
            i = 0
            serial.write(STOP_DATA_COLLECTION)

            while serial.in_waiting > 0:
                print("Collecting data", load_anim[i], end="\r")
                i = (i + 1) % 4
                collect_data(serial, raw_data)
            synthesize_data(raw_data)
            raw_data = []

        elif usr_string == 's':
            print("Not currently running... try pressing 'r'")
        elif usr_string == 'q':
            serial.write(HALT)
            break
        else:
            print("Command '{}' not recognized.  Valid commands:".format(usr_string))
            print("     c - Halt the system so the user can re-calibrate encoder positions with a level.\n"
                  "     r - Activate the controller to start running.\n"
                  "     s - Stop the controller and process data.\n"
                  "     q - Halt the system and exit.\n")


def collect_data(serial, raw_data):
    """
    @brief Read a data point from the serial line, handling a fault if necessary.
    @param serial A serial object used to communicate with the nucleo.
    @param raw_data A container for all of the data collected so far.
    """

    if serial.in_waiting == 0:
        return

    line = serial.readline()

    if line == b'FAULT':
        raise InterruptedError

    # reject data points that cross over serial read/write boundaries
    try:
        raw_data.append(json.loads(line.decode()))
    except json.decoder.JSONDecodeError:
        pass


def synthesize_data(raw_data):
    """
    @brief Convert raw data into a friendlier format for plotting then plot the data and generate a CSV
           for the current time.
    @param raw_data A List of raw data collected from the nucleo in the form of JSON dictionaries.
    """
    print("")

    # reformat the data into a format that is easier to plot
    data = clean_data(raw_data)

    if _UI_DEBUG:
        print("Found some data at times", data["time"][:5], '...')  # print some data points
        print("Data Collection completed")

    # generate plot and files
    plot_response(data)


def clean_data(raw_data):
    """
    @brief Convert raw data into a friendlier format for plotting.
    @param raw_data A List of raw data collected from the nucleo in the form of JSON dictionaries.
    @return A dictionary of the complete response, made of equal-length lists.  Units:
            - Distances [m]
            - Angles [rad]
            - Velocities [m/s]
            - Angular Velocities [rad/s]
            - Torques [N-m]
    """

    resp = {"time": [],
            "x": [],
            "theta_y": [],
            "x_dot": [],
            "theta_y_dot": [],
            "torque_x": [],
            "y": [],
            "theta_x": [],
            "y_dot": [],
            "theta_x_dot": [],
            "torque_y": []}

    for dp in raw_data:
        resp["time"].append(dp["time"])

        # x axis
        resp["x"].append(dp["x_profile"]["pos"])
        resp["theta_y"].append(dp["x_profile"]["theta"])
        resp["x_dot"].append(dp["x_profile"]["vel"])
        resp["theta_y_dot"].append(dp["x_profile"]["theta_dot"])
        resp["torque_x"].append(dp["x_profile"]["torque"])

        # y axis
        resp["y"].append(dp["y_profile"]["pos"])
        resp["theta_x"].append(dp["y_profile"]["theta"])
        resp["y_dot"].append(dp["y_profile"]["vel"])
        resp["theta_x_dot"].append(dp["y_profile"]["theta_dot"])
        resp["torque_y"].append(dp["y_profile"]["torque"])

    return resp


def plot_response(resp, loc="plots"):
    """
    @brief Create a plot for each state variable vs time for the whole response.
    @param resp The response to plot.  Expects a dictionary of lists of equal size as generated by clean_data().
    @param loc The folder in which to save the plots.  If unspecified, they will be saved to the directory this was
               called in.
    """

    # Create directory for plots
    file_time = datetime.now().strftime("%Y_%m_%d_%I-%M-%S_%p")
    location = "{}/{}".format(loc, file_time)
    os.mkdir(location)

    # Plot linear position response
    pyplot.figure(1)
    pyplot.plot(resp["time"], [x * 1000 for x in resp["x"]], label="Horizontal (x)")
    pyplot.plot(resp["time"], [y * 1000 for y in resp["y"]], label="Vertical (y)")
    pyplot.legend()
    pyplot.title("Ball Linear Position vs. Time")
    pyplot.xlabel(r'Time, $t$ (s)')
    pyplot.ylabel(r'Distance, $x$ (mm)')
    pyplot.savefig("{}/{}-{}.png".format(location, file_time, "pos-vs-time"))
    pyplot.clf()

    # Plot position trace
    pyplot.figure(5, figsize=(176 / 25.4, 100 / 25.4))
    pyplot.plot([x * 1000 for x in resp["x"]], [y * 1000 for y in resp["y"]], color='red')
    pyplot.title("Ball Position Trace")
    pyplot.xlabel(r'Horizontal Position, $x$ (mm)')
    pyplot.ylabel(r'Vertical Position, $y$ (mm)')
    pyplot.xlim([-88, 88])
    pyplot.ylim([-50, 50])
    pyplot.grid()
    pyplot.savefig("{}/{}-{}.png".format(location, file_time, "pos_trace"))
    pyplot.clf()

    # Plot Platform angle response
    pyplot.figure(2)
    pyplot.plot(resp["time"], [x * 180 / np.pi for x in resp["theta_y"]], label=r'$\theta_{y}$')
    pyplot.plot(resp["time"], [y * 180 / np.pi for y in resp["theta_x"]], label=r'$\theta_{x}$')
    pyplot.legend()
    pyplot.title(r'Platform Angle, $\theta$ vs. Time')
    pyplot.xlabel(r'Time, $t$ (s)')
    pyplot.ylabel(r'Platform Angle, $\theta$ (°)')
    pyplot.savefig("{}/{}-{}.png".format(location, file_time, "theta-vs-time"))
    pyplot.clf()

    # Plot horizontal velocity response
    pyplot.figure(3)
    pyplot.plot(resp["time"], resp["x_dot"], label=r'Horizontal $\dot{x}$')
    pyplot.plot(resp["time"], resp["y_dot"], label=r'Vertical $\dot{y}$')
    pyplot.legend()
    pyplot.title("Velocity vs. Time in both directions")
    pyplot.xlabel(r'Time, $t$ (s)')
    pyplot.ylabel(r'Velocity, $V$ (m/s)')
    pyplot.savefig("{}/{}-{}.png".format(location, file_time, "vel-vs-time"))
    pyplot.clf()

    # Plot angular velocity response
    pyplot.figure(4)
    pyplot.plot(resp["time"], resp["theta_y_dot"], label=r'$\dot{\theta}_{y}$')
    pyplot.plot(resp["time"], resp["theta_x_dot"], label=r'$\dot{\theta}_{x}$')
    pyplot.legend()
    pyplot.title(r'Platform angular velocities vs. Time')
    pyplot.xlabel(r'Time, $t$ (s)')
    pyplot.ylabel(r'Platform Angular Velocity, $\dot{\theta}$ (rad/s)')
    pyplot.savefig("{}/{}-{}.png".format(location, file_time, "theta_dot-vs-time"))
    pyplot.clf()

    # Save the response to a .json file
    with open("{}/{}-{}.json".format(location, file_time, "response"), "w") as f:
        json.dump(resp, f)


if __name__ == '__main__':
    main(sys.argv)

## @}
#
