## \addtogroup Lab0xFF
#  @{

""" @file serial_codes.py
    @brief Definitions for all of the serial line codes to be passed back and forth between the frontend and the nucleo.
    @author Tyler Olson
    @date June 9, 2021
"""

## Defines a constant for calibration mode
CALIBRATE = "CALIBRATE\n".encode("ascii")

## Defines a constant to signal the board to start the controller
RUN = "RUN\n".encode("ascii")

## Defines a constant to signal the board to terminate operation
HALT = "HALT\n".encode("ascii")

## Defines a constant for data collection start
START_DATA_COLLECTION = "START_DATA_COLLECTION\n".encode("ascii")

## Defines a constant for data collection stop
STOP_DATA_COLLECTION = "STOP_DATA_COLLECTION\n".encode("ascii")


## @}
#
