## \addtogroup Hw0x01 Hw0x01
#  @{

""" @file getChange.py
    @brief Helper function for the Vendotron project (\ref pg_lab01 )
    @author Tyler Olson
    @date April 5, 2021
"""

def getChange(price, payment):
    """
    @brief Calculate the change required based on a given payment.
    @param price A price in USD, expressed as a real number divisible by 0.01
    @param payment A dictionary containing the amount of each denomination
           that was paid.  Keys are the denomination value and Values are the
           frequency of said denomination.
    @return A dictionary of the same form as payment containing exact change in
            the fewest possible number of items.
    """
    denominations = list(payment.keys())
    i = len(denominations) - 1
    frequency = [0] * len(denominations)

    change = sum([payment[key] * key for key in payment]) - price
    if change < 0:
        return None

    while i >= 0:
        new_change = change - denominations[i]
        
        if new_change > 0:
            change = new_change
            frequency[i] += 1
        else:
            i -= 1

    return dict(zip(denominations, frequency))

##  @}
#


if __name__ == "__main__":
    payment = {
        0.01 : 7,
        0.05 : 0,
        0.10 : 3,
        0.25 : 2,
        1.00 : 4,
        5.00 : 3,
        10.00 : 0,
        20.00 : 1
    }
    price = 37.55

    change = {
        0.01 : 2,
        0.05 : 1,
        0.10 : 0,
        0.25 : 1,
        1.00 : 2,
        5.00 : 0,
        10.00 : 0,
        20.00 : 0
    }

    assert getChange(price, payment) == change
    assert getChange(100, payment) == None
    assert len(payment) == len(change)
    assert getChange(0, {1 : 0}) == {1 : 0}
