## \addtogroup Lab0x03 Lab0x03
#  @{

""" @file main.py
    @brief Contains the main finite state machine for Lab 0x03
    @author Tyler Olson
    @date May 6, 2021
"""

## @brief set a debug flag for extra messages and buffer space
#
_DEBUG = True
# Increase buffer for more helpful debugging
if _DEBUG:
    import micropython
    micropython.alloc_emergency_exception_buf(100)

import array
import Button
import utime
import pyb
from pyb import Pin, Timer, ADC

## @brief Global LD2 object for LED
#
LD2 = Pin(Pin.cpu.A5, Pin.OUT_PP)

## @brief Global A0 object for ADC counting
#
A0 = ADC(Pin.cpu.A0)

## Define a constant for data collection start
#
START_DATA_COLLECTION = "START_DATA_COLLECTION\n".encode("ascii")

## Define a constant for data collection stop
#
STOP_DATA_COLLECTION = "STOP_DATA_COLLECTION\n".encode("ascii")

## Define a constant buffer size (number of data points)
#
BUFF_SIZE = 5000

## Define a constant period in microseconds (time between data points)
#
SAMPLE_PERIOD = 10

## Any ADC reading below this is considered LOW
#
LOWER_THRESHOLD = 10  # ADC counts

## Any ADC reading below this is considered HIGH
#
UPPER_THRESHOLD = 4000  # ADC counts


class State:
    """
    @brief A container for ADC events and buffer
    @details After the state is 'started' it will poll the ADC as often as possible.  Polling is done in buckets
             of size BUFF_SIZE with samples taken once every SAMPLE_PERIOD microseconds.  Once the response is
             detected, polling is 'stopped'.
             This object can also prune buffers to the interesting region (rising edge).
    """

    def REPORT(self, message):
        """
        @brief Schedule a debug message to be reported with a time stamp.
        @param message The message to be reported.
        """
        if _DEBUG:
            micropython.schedule(self._report, [utime.ticks_diff(utime.ticks_us(), self.epoch), message])

    def _report(self, stamp):
        """
        @brief Report a debug message with a time stamp.
        @details This is a lazy helper to REPORT.
        @param stamp A tuple whose first element is a time stamp in microseconds and second is the message
        """
        time, message = stamp
        print("[{:10.3f}] {}".format(time / 1000000, message))

    def __init__(self):
        """
        @brief Initialize the state variable (only needs to be called once)
        """
        ## Set time to zero for debugging #
        if _DEBUG:
            self.epoch = utime.ticks_us()

        ## A buffer of size BUFF_SIZE in which to store ADC counts of up to 16 bit #
        self.buffer = array.array('H', [0] * BUFF_SIZE)
        ## Indicate when the rising edge has been detected #
        self.FINISHED = False
        ## A timer which starts the poll once ever 5us
        self.timer = Timer(2, freq=200000)

    def poll_ADC(self, _):
        """
        @brief Fill the buffer with ADC data and stop the FSM if a rising edge is detected.
        @details Polls the ADC at a frequency of 100kHz.  This frequency provides good resolution without duplicating
                 data points.
        @param _ Not used, but required as an interrupt.
        """
        # record values for ADC value every 10us
        A0.read_timed(self.buffer, 100000)
        if self.buffer[-1] > UPPER_THRESHOLD:
            self.stop(_)

    def prune_buffer(self):
        """
        @brief Prune the buffer to interesting region.
        @details Remove leading values below LOWER_THRESHOLD and trailing values above UPPER_THRESHOLD
        @return A smaller array of ADC counts characterizing the rising edge.
        """
        i = 0
        j = BUFF_SIZE - 1
        if _DEBUG:
            print("Before Pruning:", len(self.buffer))
        while self.buffer[i] < LOWER_THRESHOLD:
            i += 1
        while self.buffer[j] > UPPER_THRESHOLD:
            j -= 1
        if _DEBUG:
            print("After Pruning:", j + 1 - i)
            print(self.buffer[i - 1:j])
        return self.buffer[i - 1:j]

    def start(self, _):
        """
        @brief This is an interrupt that starts ADC polling to be triggered by the button's falling edge.
        """
        if _DEBUG:
            print("State started")
        self.timer.callback(self.poll_ADC)

    def stop(self, _):
        """
        @brief This is an interrupt that stops ADC polling and indicates that data collection is finished.
        """
        if _DEBUG:
            print("State stopped")
        self.timer.callback(None)
        self.FINISHED = True


def main():
    """
    @brief Entry point for button rising edge charaterization.
    @details Wait until the frontend sends the START_DATA_COLLECTION signal, then start collecting data from the user
             button.  Once the FSM indicates that data collection is finished, prune the data and send it back to the
             frontend with each datapoint timestamped.  Time stamps are CSV entries whose first element is time in
             seconds and second element is voltage in ADC counts.
    """
    # Stop sending debug data to UART
    pyb.repl_uart(None)

    # Define the serial port to write to
    serial = pyb.UART(2)

    # wait for start signal
    while serial.any() == 0 or serial.read() != START_DATA_COLLECTION:
        pass

    # start data collection
    state = State()
    button = Button.Button(falling_callback=state.start)
    button.enable()
    if _DEBUG:
        print("Ready to collect data")

    # then wait for the user to press and release the button
    while not state.FINISHED:
        pass
    button.disable()
    print("Data collection complete")

    pruned_buffer = state.prune_buffer()

    data_points = ""
    for i in range(len(pruned_buffer)):
        data_points += generate(SAMPLE_PERIOD * i / 1000000, pruned_buffer[i])
    serial.write(STOP_DATA_COLLECTION)
    serial.write(data_points.encode('ascii'))

    if _DEBUG:
        print("Data collection completed")


def generate(time_s, data):
    """
    @brief Generate a CSV entry to send to the front end of the format "time, counts".
    """

    return "{},{}\n".format(time_s, data)


if __name__ == '__main__':
    main()

## @}
#
