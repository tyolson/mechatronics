## \addtogroup Lab0x03 Lab0x03
#  @{

""" @file lab3/UI_front.py
    @brief Contains the front-end User Interface for interacting with the Nucleo.
    @details Currently only supported on Linux environments (64-bit Ubuntu 20.04.1)
    @author Tyler Olson
    @date May 6, 2021
"""
import os
import time
from datetime import datetime
import numpy as np

from matplotlib import pyplot
from serial import Serial

## Define a constant for data collection start
#
START_DATA_COLLECTION = "START_DATA_COLLECTION\n".encode("ascii")

## Define a constant for data collection stop
#
STOP_DATA_COLLECTION = "STOP_DATA_COLLECTION\n".encode("ascii")

## @brief set a debug flag for extra messages and buffer space
#
UI_DEBUG = True


def main():
    """
    @brief Run the frontend user interface for determining the time constant of the Nucleo's user button.
    @details Launches a terminal-based UI.  If the user presses ‘G,’ the Nucleo should begin sampling data.
             When data collection is complete, the script generates both an image (using matplotlib) and a .CSV
             file of data (timestamps and data readings).  The plot also reports the time constant of the rising edge.
             The environment variable ST_LINK must be set to the port that serial communication will be executed on.
    """

    # Define Serial port as ST-LINK #
    serial = Serial(port=os.environ['ST_LINK'], baudrate=115273, timeout=1)

    # wait for the user to press 'G'
    while input("Press 'G' to begin data collection: ") != 'G':
        pass

    # Send the start code
    serial.write(START_DATA_COLLECTION)

    # wait 30 seconds for the user to press the user button
    print("Press and release the User Button to capture data")

    epoch = time.time()
    timeout = 30
    while time.time() - epoch < timeout:
        line = serial.readline()
        if line == STOP_DATA_COLLECTION:
            break

    raw_data = serial.readlines()
    if UI_DEBUG:
        print(raw_data[:10], '...')  # print some data points

    # remove any data points that were misread and reformat them
    data_points = clean_data(raw_data)
    if UI_DEBUG:
        print(data_points[:10], '...')  # print some data points

    # generate plot and files
    file_time = datetime.now().strftime("%Y_%m_%d_%I-%M-%S_%p")
    gen_csv("data/data{}.csv".format(file_time), data_points)
    gen_image("plot/plot{}.png".format(file_time), data_points)
    serial.close()


def clean_data(raw_data):
    """
    @brief Reformat the raw data into a list of strings.
    @details No longer rejects malformed data points.
    @param raw_data Raw data in the form of an array.array of bytes
    """
#    regex = re.compile(r"\d+.\d+, \d+")
    cleaned_data = []
    for line in raw_data:
        clean_line = line.decode('ascii').strip()
#       if regex.match(clean_line):
        cleaned_data.append(clean_line)
    return cleaned_data


def gen_csv(filename, data):
    """
    @brief Generate a .CSV file from the given data.
    @param filename Filename to write the .CSV file into
    @param data Data (a List of strings) to write to the CSV file
    """
    with open(filename, "w") as f:
        for line in data:
            f.write(line + '\n')


def calc_time_constant(times, values):
    """
    @brief Calculate the time constant of a button rising edge.
    @param times A List of time stamps in seconds, must be the same length as values
    @param values A List of values of ADC counts
    """

    ln_vals = []
    for i in range(len(values)):
        try:
            # Calculate ln(Vdd - Vc)
            ln_vals.append(-np.log(4096 - values[i]))
        except RuntimeError:
            # if ln is unsolvable, take the last value
            ln_vals.append(ln_vals[-1])

    # perform a linear regression to get slope
    slope, _ = np.polyfit(times, ln_vals, 1)
    print("slope:", slope)
    # m = 1/tau
    return 1 / slope


def gen_image(filename, data):
    """
    @brief Generate a plot of user button rising edge response.
    @details Plots Time(s) vs. Voltage (ADC counts) and reports the Time Constant
    @param filename The name of the file to write plot to
    @param data A list of strings of the form "time,value" to be plotted
    """
    times = []
    values = []

    # format data
    for line in data:
        print(line.split(','))
        tim, value = line.split(',')
        times.append(float(tim))
        values.append(int(value))

    # generate plot
    pyplot.figure()
    pyplot.plot(times, values)
    pyplot.title("User Button Rising Edge Response Time vs. Voltage")
    pyplot.xlabel('Time (s)')
    pyplot.ylabel('Voltage (ADC counts)')
    pyplot.text(0, 0, "     Time Constant: {:.3e}".format(calc_time_constant(times, values)))
    pyplot.savefig(filename)


if __name__ == '__main__':
    main()

## @}
#
