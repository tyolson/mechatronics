## \addtogroup Lab0x03
#  @{

""" @file lab3/Button.py
    @brief Button class for detecting button actions
    @details This button was largely copied from a Simon Says project from ME 305, however with minor changes in
             functionality.  Now it simply reports button actions and can call functions passed in by its creator
             upon rising and falling edges.
    @author Tyler Olson
    @date May 6, 2021
"""

import utime
import micropython
from pyb import Pin, Timer, ExtInt
from main import LD2, _DEBUG


class Button(Pin):
    """
    @brief A Button class that reports rising and falling edges.
    @details This button class extends the pyb.Pin class, so can be treated as a Pin.  On creation the button can be
             given callback functions to call on rising and calling edges.  It can also be easily enabled and disabled.
    """

    def REPORT(self, message):
        """
        @brief Schedule a debug message to be reported with a time stamp.
        @param message The message to be reported.
        """
        if _DEBUG:
            micropython.schedule(self._report, [utime.ticks_diff(utime.ticks_us(), self.epoch), message])

    def _report(self, stamp):
        """
        @brief Report a debug message with a time stamp.
        @details This is a lazy helper to REPORT.
        @param stamp A tuple whose first element is a time stamp in microseconds and second is the message
        """
        time, message = stamp
        print("[{:10.3f}] {}".format(time / 1000000, message))

    def disable(self, trigger: Timer = None):
        """
        @brief Disable the button and runs the callback function specified upon Button creation.
        @param trigger The timer that disabled the button (if supplied)
        """
        if trigger:
            trigger.deinit()
        self.interrupt.disable()
        self.REPORT("Button Disabled (pin {})".format(self.pin()))

    def enable(self):
        """
        @brief Enable the button after having been disabled.
        """

        self.interrupt.enable()
        self.triggered = Pin.IRQ_RISING
        self.REPORT("Button Enabled (pin {})".format(self.pin()))

    def trigger(self, irq_src: int):
        """
        @brief Collect and report a button action.
        @details Currently the debug functionality in unstable within interrupts, so it has been disabled.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        # update the current status and perform appropriate action
        if self.triggered == Pin.IRQ_RISING:
#            if _DEBUG:
#                print("Button Pressed (pin 13)")
            self.press(irq_src)
        else:
#            if _DEBUG:
#                print("Button Released (pin 13)")
            self.release(irq_src)
#        print("[{:10.3f}] {}".format(utime.ticks_diff(utime.ticks_us(), self.epoch) / 1000000, message))

    def press(self, irq_src: int = None):
        """
        @brief Recognizes the button has been pressed and updates the time.
        @details Sets time_pressed and, if applicable, schedules a callback function with the triggered time.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        LD2.high()
        self.triggered = Pin.IRQ_FALLING
        self.time_pressed = utime.ticks_diff(utime.ticks_us(), self.epoch)

        # launch the callback function for main to handle (if it exists)
        if self.callback['falling'] is not None:
            micropython.schedule(self.callback['falling'], self.time_pressed)

    def release(self, irq_src: int = None):
        """
        @brief Recognizes the button press has finished.  Updates the sequence with the type of press.
        @details Sets time_released and, if applicable, schedules a callback function with the triggered time.
        @param irq_src The interrupt request source line number (required as an interrupt callback).
        """

        LD2.low()
        self.triggered = Pin.IRQ_RISING
        self.time_released = utime.ticks_diff(utime.ticks_us(), self.time_pressed)

        # launch the callback function for main to handle (if it exists)
        if self.callback['rising'] is not None:
            micropython.schedule(self.callback['rising'], self.time_pressed)

    def __init__(self, falling_callback=None, rising_callback=None, epoch=None):
        """
        @brief Create a Button object.
        @param falling_callback A function to optionally be called when a falling edge is detected
        @param rising_callback A function to optionally be called when a rising edge is detected
        @param epoch A global zero time to which all other time readings are compared.  If not provided, the button will
               create its own.
        """

        super().__init__(Pin.cpu.C13, Pin.IN, Pin.PULL_NONE)
        if epoch:
            ## Compare all timed events to this time (button zero). #
            self.epoch = epoch
        else:
            self.epoch = utime.ticks_us()

        ## Interrupt for button detection and timeout handling #
        self.interrupt = ExtInt(self.name(), mode=ExtInt.IRQ_RISING_FALLING, pull=self.pull(), callback=self.trigger)

        ## The last edge that was encountered on the button
        #  The default state is that the button has not been pressed (i.e. previous edge was RISING).
        #
        self.triggered = Pin.IRQ_RISING
        ## Last time the button was pressed #
        self.time_pressed = None
        ## Last time the button was released #
        self.time_released = None

        ## Callback functions to return to when the button is pressed or released #
        self.callback = {'falling': falling_callback,
                         'rising': rising_callback}

## @}
#
