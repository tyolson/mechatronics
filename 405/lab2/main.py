## \addtogroup Lab0x02 Lab0x02
#  @{

""" @file main.py
    @brief Contains the main state machine for Lab 0x02
    @author Tyler Olson
    @date April 30, 2021
"""

## @brief set a debug flag for extra messages and buffer space
#
_DEBUG = False
# Increase buffer for more helpful debugging
if _DEBUG:
    import micropython
    micropython.alloc_emergency_exception_buf(200)

import Button
import utime
from random import randint
from pyb import Pin, Timer

## @brief Global LD2 object for LED
#
LD2 = Pin(Pin.cpu.A5, Pin.OUT_PP)

## @brief Global B2 object for interrupt triggering
#
B3 = Pin(Pin.cpu.B3, Pin.IN)


class State:
    """
    @brief A class to hold the current state of the FSM.
    @details Sets up a timer with 1us divisions that responds to interrupts.  Responsible for cycling the LED and
             recording reaction times.
    """

    def __init__(self):
        """
        @brief Initialize the state variable (only needs to be called once).
        @details Starts a timer to cycle the LED on and off, and record reaction times in between.
        """
        self.epoch = utime.ticks_us()

        ## Time (in counts) that the LED is turned on.
        #
        self.trigger_time = None

        ## Reaction time, by number of counts.
        #
        self.delta = None

        ## @brief User button object.
        #  @details The button triggers the state to update its time delta value.  This button is synchronized with the
        #  state clock.
        #
        self.button = Button.Button(self.set_delta, self.epoch)

        ## @brief The timer that interrupts are based on.
        #  @details A timer with a period of 4s with 1us divisions, with two interrupt channels.
        #           Channel 1 is used for Output Control (Toggling the LED and changing state).
        #           Channel 2 is used for Input Capture (triggers when the user button is pressed).
        #
        self.timer = Timer(2, prescaler=80, period=3999999)
        self.ch1 = self.timer.channel(1, Timer.OC_ACTIVE, callback=self.tick_test)
        self.ch2 = self.timer.channel(2, Timer.IC, pin=B3, polarity=Timer.FALLING, callback=self.button.press)

        ## A collection of all the reaction times
        #
        self.reaction_times = []

    def set_delta(self, time_us):
        """
        @brief Respond to a user button and set the time delta, in timer counts.
        @param time_us The time in microseconds (timer counts) since the trigger was set.
        """
        self.delta = time_us - self.trigger_time
        if _DEBUG:
            time = utime.ticks_diff(utime.ticks_us(), self.epoch) / 1000000
            print("[{:10.3f}] Reaction time: {:7.6f} seconds".format(time, self.delta / 1000000))

        # disable the button
        # self.ch2.callback(None)

    def record_reaction(self):
        """
        @brief An asynchronous function to record the last reaction time.
        @details This function will record the current state to a list of reaction times.
        """
        if self.delta:
            self.reaction_times.append(self.delta / 1000000)
        self.delta = None

    def tick_test(self, timer):
        """
        @brief Upon update from the timer, move the state forward by one step.
        @details If the LED is on, turn it off and set the OC timer to trigger randomly between 2-3s.
                 If the LED is off, turn it on for 1 second and set the trigger starting time to now.
        @param timer Unused placeholder to respond as an interrupt.
        """
        if LD2.value():
            LD2.low()
            # disable the button if it is not already
            # self.ch2.callback(None)

            # set next interrupt to either 2-3 seconds
            self.ch1.compare((self.ch1.compare() + randint(2, 3) * 1000000) % 4000000)
        else:
            LD2.high()
            # enable the button
            # self.ch2.callback(self.button.press)
            self.trigger_time = self.ch1.compare()

            # set next interrupt to exactly second from now
            self.ch1.compare((self.ch1.compare() + 1000000) % 4000000)


def _mean(values):
    """
    @brief A helper function that calculates the average of a list of real values.
    @param values The list to be averaged.
    """
    if len(values) > 0:
        return sum(values) / len(values)
    return 0


def main():
    """
    @brief Entry point for the Reaction Timing test lab.
    @details Initialize the FSM and record a reaction whenever it exists.  If the User presses CTRL+C (sends a
             Keyboard Interrupt) then report the average reaction time and exit gracefully.
    """
    state = State()
    try:
        while True:
            state.record_reaction()
    except KeyboardInterrupt:
        print("Average reaction time:", _mean(state.reaction_times))


if __name__ == '__main__':
    main()

## @}
#
