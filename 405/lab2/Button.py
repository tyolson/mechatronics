## \addtogroup Lab0x02 Lab0x02
#  @{

""" @file lab2/Button.py
    @brief Button helper class for grabbing reaction times.
    @details This button was largely copied from a Simon Says project from ME 305, however with minor changes in
             functionality.  Now instead of reporting the duration of each button press, it simply reports the precise
             time of presses (and releases) to its creator (if requested).
    @author Tyler Olson
    @date April 30, 2021
"""

import utime
import micropython
from pyb import Pin, Timer
from main import _DEBUG


class Button(Pin):
    """
    @brief A button class that reports precisely when it is pressed.
    @details Subclasses the Pin object from pyb.  Responds upon the falling edge (push) of the user button.
    """

    def press(self, timer):
        """
        @brief Recognizes the button has been pressed and records it.
        @details If applicable, the Button's callback function is also called.
        @param timer The timer which triggered this interrupt.
        """

        self.pressed_counts = timer.channel(2).capture()
        if _DEBUG:
            print("Button pressed at:", self.pressed_counts)

        # launch the callback function for main to handle (if it exists)
        if self.callback is not None:
            micropython.schedule(self.callback, self.pressed_counts)

    def __init__(self, callback=None, epoch=None):
        """
        @brief Create a Button object.
        @param callback A function to optionally be called once the button is disabled
        @param epoch The starting time of the owner of this Button.  If provided, the button will synchronize with its
                     creator.
        """

        super().__init__(Pin.cpu.C13, Pin.IN, Pin.PULL_NONE)
        if epoch:
            self.epoch = epoch
        else:
            self.epoch = utime.ticks_us()

        ## The time when this button is pressed.
        #
        self.pressed_counts = None

        ## A function to return to when the button is pressed.
        #
        self.callback = callback

## @}
#
