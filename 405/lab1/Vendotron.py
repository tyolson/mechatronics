## \addtogroup Lab0x01 Lab0x01
#  @{

""" @file Vendotron.py
    @brief Runs a vending machine emulator in the terminal.
    @author Tyler Olson
    @date April 22, 2021
"""

import time
from random import randint
import keyboard


class Drink:
    """
    @brief A small struct representing a drink
    """

    def __init__(self, name, price):
        """
        @brief Create a Drink object.
        @param name The name of the drink
        @param price The price of the drink in USD (must be a float)
        """
        self.name = name        ##< The name of the drink #
        self.price = price      ##< The price of the drink #


class Currency:
    """
    @brief A small struct representing a denomination of US currency
    """

    def __init__(self, key, value):
        """
        @brief Create a Currency object.
        @param key The keyboard key to trigger inserting one of these (must be a string that describes a key name)
        @param value The value of the currency in USD (must be a float)
        """
        self.key = key          ##< The keyboard key that triggers inserting this Currency #
        self.value = value      ##< The value of this currency #

    @staticmethod
    def currency_list(bitmap):
        """
        @brief Generate a list of accepted denominations from all possible US denominations.
        @param bitmap A bitmap (int) where high bits indicate selection of the denomination in lexical order
        @return A list of currency objects, sorted in ascending order by value
        """
        US_denoms = [0.01, 0.05, 0.10, 0.25, 0.50, 1.00, 2.00, 5.00, 10.00, 20.00, 50.00, 100.00]
        return [Currency(i, US_denoms[-(i + 1)]) for i in range(len(US_denoms)) if bitmap & (2 ** i)][::-1]


## @brief A dictionary of all drinks in the machine.
#  @details Keys are the keyboard key that triggers this drink.
#  Values are drink objects.
#
drinks = {'c': Drink('Cuke', 1.00),
          'p': Drink('Popsi', 1.20),
          's': Drink('Spryte', 0.85),
          'd': Drink("Dr. Pupper", 1.10)}


def display(message):
    """
    @brief Display a Vendotron message in a pretty format.
    @param message The message to be displayed
    """

    print("     |{:^50}| ".format(message), end="\r")


def get_change(price, payment):
    """
    @brief Calculate the change required based on a given payment.
    @param price A price in USD, expressed as a real number divisible by 0.01
    @param payment A dictionary containing the amount of each denomination
           that was paid.  Keys are the denomination value and Values are the
           frequency of said denomination.
    @return A dictionary of the same form as payment containing exact change in
            the fewest possible number of items
    """
    denominations = list(payment.keys())
    i = len(denominations) - 1
    frequency = [0] * len(denominations)

    change = sum([payment[key] * key for key in payment]) - price
    if change < 0:
        return None

    while i >= 0:
        new_change = change - denominations[i]

        if new_change > 0:
            change = new_change
            frequency[i] += 1
        else:
            i -= 1

    return dict(zip(denominations, frequency))


class State:
    """
    @brief The current state of the Vendotron.
    """
    ## The message currently displayed
    message = None
    ## A list of acceptable Currencies
    currencies = Currency.currency_list(0b111101011101)
    ## Frequencies of payments that have been made
    payment = None
    ## Total balance
    balance = None
    ## Indicates if the Vendotron is currently running
    RUNNING = False
    ## Timestamp of the state change
    epoch = time.time()

    @staticmethod
    def initialize():
        """
        @brief (Re)initialize the vendotron to its starting state.
        @details Set the frequency of all payments and balance to zero, display the welcome message, and set RUNNING to
                 True.
        """

        State.payment = {c.value: 0 for c in State.currencies}
        State.balance = 0.00
        State.message = "Welcome to Vendotron!"
        State.RUNNING = True

    @staticmethod
    def drink_select(drink):
        """
        @brief Dispense a drink if the balance is high enough, otherwise display the price.
        @param drink The drink to attempt to purchase
        """

        if drink.price > State.balance:
            State.message = "Insufficient Funds.  Drink price: ${:.2f}".format(drink.price)
        else:
            State.dispense_drink(drink)

    @staticmethod
    def dispense_drink(drink):
        """
        @brief Dispense a drink.
        @details Dispense a drink and deduct the drink's cost from the balance.  If there is any leftover prompt the
                 user to buy another.
        """

        display("Here's that {} you wanted!".format(drink.name))
        time.sleep(1.0)

        # animate the drink rolling
        message = "{:>50}".format("(o)")
        for i in range(47):
            message = message[1:] + message[0]
            time.sleep(0.05)
            display(message)
        State.balance -= drink.price
        if State.balance > 0:
            State.message = "Still thirsty? Try another refreshing beverage!"

    @staticmethod
    def insert_currency(index):
        """
        @brief Insert one Currency into the Vendotron and display the current balance.
        @param index The index in the currencies list of which currency is being inserted
        """

        currency = State.currencies[index]
        State.payment[currency.value] += 1
        State.balance += currency.value
        State.message = "Current Balance: {:.2f}".format(State.balance)

    @staticmethod
    def eject():
        """
        @brief Return the current balance to the user in the fewest number of coins possible.
        @return A dictionary containing quantities of each currency where keys are the currency value and values are the
                frequency of each denomination.
        """

        change = get_change(0, State.payment)
        State.initialize()
        return change

    @staticmethod
    def terminate():
        """
        @brief Stop the Vendotron and exit the program.
        """
        State.RUNNING = False


def handle_key(key):
    """
    @brief Catch a key press and do the appropriate action
    @details 0-7: insert a currency of one of these denominations:
              * 0 - penny
              * 1 - nickle
              * 2 - dime
              * 3 - quarter
              * 4 - 1 dollar bill
              * 5 - 5 dollar bill
              * 6 - 10 dollar bill
              * 7 - 20 dollar bill
            c, p, s, d: select a drink from \ref drinks
            e: eject the current balance
            q: terminate the program
    """

    if key.name.isdigit() and int(key.name) < 8:
        State.insert_currency(int(key.name))
    elif key.name in drinks:
        State.drink_select(drinks[key.name])
    elif key.name == 'e':
        State.eject()
    elif key.name == 'q':
        State.terminate()
    else:
        pass
    print('', end="\r")


def main():
    """
    @brief Entry point for the Vendotron program.
    @details Run the Vendotron program, first displaying a welcome message and then responding to user actions.
             If the user hasn't pressed any buttons in a while (30s), display a random slogan.
             For user actions see \ref handle_key .
             Also accepts CTRL+c to terminate the program swiftly.
    """

    # Set key Bindings
    keyboard.on_release(callback=handle_key)

    # Print welcome message
    print("Wemcome to Vendotron!  Try a tasty drink:")
    for key, drink in drinks.items():
        print(" * {} for {:<13} only ${:.2f}!".format(key.upper(), drink.name, drink.price))
    print("Press q to quit at any time or E to eject.\n")

    State.initialize()
    messages = ["Wow, wouldn't you just love a Spryte?",
                "Who likes Popsi?  Everyone!!!",
                "Vendotron: the coolest vendor on the block."]

    try:
        last_message = None
        while State.RUNNING:
            if State.message != last_message:
                display(State.message)
                last_message = State.message
                State.epoch = time.time()
            elif time.time() - State.epoch > 30:
                State.message = messages[randint(0, 2)]
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
    finally:
        # flush all keystrokes
        keyboard.send("enter")
        input()


if __name__ == "__main__":
    main()

##  @}
#
