## @mainpage ME 405 Portfolio
#  \tableofcontents
#  This is the main page for Tyler Olson's ME 405 (Spring 2021) portfolio.  Further documentation can be accessed from
#  the sidebar or from the links in each assignment description below.
#
#
#  @section sec_lab Labs
#  Lab Modules:
#  * \ref pg_lab01 : Reintroduction to Python and Finite State Machines
#
#  @subsection sec_lab1 Lab0x01
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab1/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab1/</u></a>
#  * Documentation: \ref pg_lab01
#
#  @subsection sec_lab2 Lab0x02
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab2/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab2/</u></a>
#  * Documentation: \ref pg_lab02
#
#  @subsection sec_lab3 Lab0x03
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab3/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab3/</u></a>
#  * Documentation: \ref pg_lab03
#
#  @subsection sec_lab4 Lab0x04
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab4/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab4/</u></a>
#  * Documentation: \ref pg_lab04
#
#  @subsection sec_labFF Lab0xFF
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/labFF/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/labFF/</u></a>
#  * Documentation: \ref pg_labFF
#
#  <hr>
#  @section sec_hw Homeworks
#  Below are homeworks that have associated documentation.  All other homeworks were submitted via pdf and are not
#  included in this portfolio.
#  * \ref Hw0x01 : Python Review
#
#  @subsection sec_hw1 Homework0x01
#  See \ref Hw0x01 documentation.
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/homework1/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/homework1/</u></a>
#  * Documentation: \ref Hw0x01
#
#  @subsection sec_hw2 Homework0x02
#  See Hw0x02 documentation.
#  * Source: None
#  * Documentation: \ref pg_hw2
#
#  @subsection sec_hw4 Homework0x04
#  See Hw0x04 documentation.
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/homework4/">
# #  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/homework4/</u></a>
#  * Documentation: \ref pg_hw4
#
#  @subsection sec_hw5 Homework0x05
#  See Hw0x05 documentation.
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/homework5/">
# #  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/homework5/</u></a>
#  * Documentation: \ref pg_hw5
#
#  @author Tyler Olson
#  @date May 15, 2021
#

## @page pg_lab01 Lab0x01
#  Lab 0x01: Reintroduction to Python and Finite State Machines
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab1/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab1/</u></a>
#  * Module Documentation: \ref Lab0x01
#
#  In this lab we implement a finite state machine to run Vendotron: a virtual vending machine that runs in a terminal.
#  The user can select drinks and pay for them using the keyboard.  The Vendotron is based on the assignment that was
#  started in \ref Hw0x01 and the homework solution will be used as within this project.
#
#  Note that this program must be run from root on Linux as it makes use of the keyboard module.
#
#  Below is a state transition diagram for the Vendotron, developed in Hx0x00.
#  @image html lab1_State_Diagram.jpg width=60%
#
#  @author Tyler Olson
#
#  @date April 21, 2021
#

## @page pg_lab02 Lab0x02
#  Lab 0x02: Think Fast!
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab2/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab2/</u></a>
#  * Module Documentation: \ref Lab0x02
#
#  In this lab, interrupts are used to schedule an LED and record how long it takes the user to react.  The board will
#  keep testing indefinately until the user sends a keyboard interrupt, at which point all of the reaction times are
#  averaged and reported to the user.  The existing code represents Part B of the lab which implements the interrupts
#  using the Timer module and TIC/TOC rather than the External Interrupt module.
#
#  While the method in Part B is potentially more accurate since there is less time between when the interrupt occurs
#  and it is handled, differences between the two were roughly less than 100us, 3 orders of magnitude smaller than the
#  average times.  Implementation was much easier in Part A, and it was easier to separate concerns between the Button
#  and the State objects.  If we were measuring on a smaller time scale (<1ms) then the method in Part B would
#  likely be more accurate.
#
#  @author Tyler Olson
#
#  @date April 30, 2021
#

## @page pg_lab03 Lab0x03
#  Lab 0x03: Pushing the Right Buttons
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab3/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab3/</u></a>
#  * Module Documentation: \ref Lab0x03
#
#  In this lab we characterize the rising edge response of the user button on the Nucleo.  The user interfaces with the
#  Nucleo from a frontend terminal which prompts the user to press 'G' to start data collection.  The user must then
#  press and release the button and the analog response is recorded and sent back to the frontend.  Note that a jumper
#  must be placed between pin C13 and A0 as pin C13 does not have an Analog-to-Digital converter.  The response is then
#  analyzed and plotted by the frontend, the time constant reported, and data stored in a .CSV file.
#
#  Although I was initially planning on using an interrupt-based method for signalling when the Nucleo should begin and
#  end data collection, this proved to be far too slow and the data had a poor and unpredictable resolution.  Now I am
#  using a 'bucket collection' technique where the ADC is sampled into a bucket, checked for a response, and resampled
#  until a response if found.
#
#  Note that this program makes use of an environment variable 'ST_LINK' to determine what port to perform serial
#  communication on.
#
#  Below is one of the highest-fidelity runs of data collected.  On average the time constant was around 0.5ms.  This is
#  slightly larger than the 0.48ms that would be predicted by RC analysis, however it is within 2% which is comparable
#  to the error in resistor and capacitor ratings.  Additionally there is a consistently delayed response at the front
#  of each rising edge, indicating that there are additional poles in the system and it is not simply a PT1 response,
#  as assumed by the RC simplification.
#  @image html plot2021_05_06_01-44-17_AM.png width=60%
#  The data file for this run can be accessed in the data folder in the source code.
#
#  @author Tyler Olson
#
#  @date May 6, 2021
#

## @page pg_lab04 Lab0x04
#  Lab 0x04: Hot or Not?
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/lab4/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/lab4/</u></a>
#  * Module Documentation: \ref Lab0x04
#
#  In this lab we explored the I2C communication protocol by reading temperature values from the mcp9808 temperature
#  sensor made by Microchip Technology Inc.  Temperature data was collected for a period of 9 hours and 7 minutes
#  overnight in Perfumo Canyon once every minute.  A driver class, mcp9808.TempSensor , was created to abstract the
#  details of the I2C protocol.
#
#  Shown below, the core temperature refers to temperature reported
#  by ADC channel 16 and sensor temperature refers to temperature read from the mcp9808.
#  @image html lab4_plot.png width=40%
#
#  Temperature data was saved to <a href="temps_log.csv"><u>temps_log.csv</u></a> on the nucleo and then exported
#  manually to the PC where it was plotted using the generate_plot.py utility.
#  In this lab, we also created a small utility script core_temp.py for logging the core temperature to a file.  Note
#  that this file is included here, but was not used as part of the main project.

#  @author Tyler Olson
#
#  @date May 13, 2021
#

## @page pg_labFF Lab0xFF
#  Lab 0xFF: Term Project
#  * Source: <a href="https://bitbucket.org/tyolson/mechatronics/src/main/405/labFF/">
#  <u>https://bitbucket.org/tyolson/mechatronics/src/main/405/labFF/</u></a>
#  * Module Documentation: \ref Lab0xFF
#  * Example video: <a href="example_video.mp4" target="_blank"><b>Click Here</b></a>
#
#  @section sec_intro Introduction
#  This is the culminating lab for ME 405, integrating controller results from homeworks, communication protocols from
#  previous labs, and integration on the ME 405 project board.  The goal of the term project is to balance a ball in the
#  center of a moving platform.  This is accomplished with two motors (one for each angular degree of freedom), encoders
#  and a touch panel sensor to get ball and panel locations and velocities.  While unimplemented in the term project,
#  in theory the encoder data could be replaced with readings from the IMU without a change in response.  This lab was
#  completed in several stages, each with their own detailed documentation and testing procedures.
#
#  @section sec_arc Architecture
#  This lab is organized into main code, plots, drivers, and tests.  The main code consists of:
#  * \ref main.py : The entry point and backend UI interface.  This file is also responsible for updating the system
#                   state.
#  * \ref axis.py : Each axis of the platform is controlled independently, thus they are separated in the code.  Each
#                   Axis object contains the controller and encoder associated with the primary direction (for instance
#                   the x-axis contains the motor that changes theta_y of the platform) in addition to a Profile object
#                   that tracks the current kinematics of the system (positions and velocities).
#  * \ref controller.py : Each controller is implemented will full state feedback using the gains calculated in
#                         \ref pg_hw5.  Controllers hold and update the duty cycle for the motor on their axis.
#  * \ref serial_codes.py : A file shared by frontend and backend for establishing serial communication protocols.
#  * \ref UI_front.py : The frontend program used to interact with the board from a desktop computer.
#
#  Drivers (in the /drivers module) are responsible for interacting with physical hardware and are described in more
#  detail in the \ref sec_progress section below.
#  * \ref Motor.py : Interacts with the Texas Instruments DRV8847 motor driver and both Maxon DCX22S motors.
#  * \ref Encoder.py : Maintains the true position of each US Digital E4T-1000 encoder.
#  * \ref TouchPanel.py : Allows for reading the position of a touch on the mounted Adafruit 8.5" Resistive Touch Panel.
#
#  All acquired data is saved in the /plots directory in a folder corresponding to the time of data collection.  The
#  following plots are generated each run (see \ref sec_ch5 for examples):
#  * Linear Position vs. Time
#  * Position Trace (shows where the ball moved relative to the platform surface)
#  * Platform Angle vs. Time
#  * Linear Velocity vs. Time (separated into horizontal and vertical components)
#  * Platform Angular Velocity vs. Time (separated by axis)
#  * Additionally a .json file is created containing all of the collected data.
#
#  Finally the /test directory contains files used for testing and calculating timing.
#
#  @section sec_progress Lab Progress
#  @subsection sec_ch1 Chapter 1: Reading from Resistive Touch Panels
#  The first task for this lab was determining the coordinate position of the ball by reading from a touch panel.
#  More details on testing, calibration, and driver capabilities in \ref TouchPanel.py.
#
#  @subsection sec_ch2 Chapter 2: Driving DC Motors with PWM and H-Bridges
#  The second task is to drive each motor independently using the Texas Instruments DRV8847 motor driver.  Most of the
#  code from this section was copied from the driver developed for ME 305, with added functionality related to
#  overcurrent protection.  See \ref Motor.py for additional details.
#
#  @subsection sec_ch3 Chapter 3: Reading from Quadrature Encoders
#  The third task required is developing a driver to interface with the encoders attached to each motor.  All of the
#  functional code for this driver was copied from ME 305 with some minor changes in the documentation.
#  See \ref Encoder.py for additional details.
#
#  @subsection sec_ch4 Chapter 4: Reading from an Inertial Measurement Unit
#  Due to time constraints I elected to not use the IMU.  While it may have provided more accurate measurements of the
#  platform angle, the system has fair stability reading from the encoders.  One major benefit of using the IMU that I
#  was unable to account for is having to level the platform each time before running the controller.  I was forced to
#  implement a calibration phase where the user levels the platform manually to start before placing the ball on it.
#
#  @subsection sec_ch5 Chapter 5: Balancing the Ball
#  The bulk of the work on this lab was in integrating the sensors and performing UI operations.  Although some sample
#  code was provided to aid in task scheduling, I decided to implement my own scheduling scheme for code readability and
#  significant performance gains.  After some task analysis, I determined that there are only really two separate tasks
#  that need to be run in parallel: 1) Updating the State and controller then sending data, and 2) Recieving user
#  inputs.  Because the first task can be run sufficiently quickly (at 30Hz) velocity measurements and encoder
#  readings stay updated.  Additionally, because it is much faster to read both Touch Panel positions at once, both
#  axies have the same time for each reading, weakening any gains from parallelization.  Therefore, while velocity
#  calculation and controller updates could be performed asynchronously, they are performed at the same time.  Doing so
#  not only has the added benefit of code simplicity, but also helps to reduce parasitic delay.
#  For the first task a linear logic model is then adopted:
#  1) Capture the state by reading Touch Panel positons, Encoder positions, then calculating velocities for both axies.
#  2) Update each axis' controller with the newly captured state
#  3) Write a data point to the serial line consisting of:
#       - Linear Position
#       - Linear Velocity
#       - Platform Angle
#       - Platform Angular Velocity
#       - Motor torque
#     for both axies.
#  The second task simply polls the serial line and sets the system state accordingly.  This task is extremely fast and
#  is run asynchronously when ever the first task is not being run.  This task also reports to the frontend if a motor
#  fault has been detected.
#
#  Below is a task diagram to illustrate this program flow:
#  @image html LabFF_task_diagram.png
#
#  Below is a state diagram for the backend.  For more information see \ref main.py .
#  @image html LabFF_backend_state_diagram.png width=40%
#
#  This scheme also helps to simplify data collection.  Below are plots generated from a test run of the system.
#  Data collected for this run can be referenced in
#  <a href="2021_06_09_10-49-16_PM-response.json" target="_blank"><b>response.json</b></a>.
#
#  @image html 2021_06_09_10-49-16_PM-pos_trace.png
#  <CENTER><table>
#  <tr><td>@image html 2021_06_09_10-49-16_PM-pos-vs-time.png </td>
#      <td>@image html 2021_06_09_10-49-16_PM-theta-vs-time.png </td></tr>
#  <tr><td>@image html 2021_06_09_10-49-16_PM-vel-vs-time.png </td>
#      <td>@image html 2021_06_09_10-49-16_PM-theta_dot-vs-time.png </td></tr>
#  </table></CENTER>
#
#  <a href="example_video.mp4" target="_blank"><b>Click Here</b></a> for an example video illustrating the frontend flow
#  and data collection routine.  Below is a state diagram illustrating the control flow for the frontend.
#  @image html LabFF_frontend_State_Diagram.png width=40%
#
#  @author Tyler Olson
#
#  @date June 10, 2021
#

## @page pg_hw2 Homework0x02
#  Homework 0x02: System Modeling
#  * PDF Scan: <a href="hw2_SCAN.pdf" target="_blank"><b>Click Here</b></a>
#
#  This homework contains the analysis introducing the term project: a ball balancing platform.  A 2x2 matrix is
#  developed using kinetics and kinematics for a simplified system to define the relationship between motor torque,
#  plate angular acceleration, and linear acceleration of the ball in one direction.  These calculations simplify the
#  system with the following major assumptions:
#  * Rotations of the platform are independent, thus modeled as two independent rotating beams.
#  * All bodies are rigid.
#  * 2D Kinematics: the motor lies in the same plane as the platform rotation it effects.
#  * Ball rolls without slipping.
#  * Masses of the servo arm and link are negligible.
#
#  @image html hw2_System_CAD.png width=60%
#
#  @author Tyler Olson
#
#  @date April 21, 2021
#

## @page pg_hw4 Homework0x04
#  Homework 0x04: Symulation or reality?
#  * Jupyter Notebook: <a href="Homework4.ipynb" target="_blank"><b>Click Here</b></a>
#  * Static HTML: <a href="Homework4.html" target="_blank"><b>Click Here</b></a>.  Note, due to the way Jupyter converts
#    notebooks to HTML, this page may take a while to load.
#
#  This homework builds upon the analysis in \ref pg_hw2 for the balancing ball platform term project.  In this step,
#  the state equations previously developed are put into standard form and linearized by finding the Jacobian
#  approximation of the system with respect to the state vector and the input vector (the torque applied by the servo).
#  I adapted Charlie's solution matrix (that was posted on piazza) to take a form similar to the solution I derrived
#  previously.  For the purpose of error checking, I used this modified solution.  Additionally I was unsure how to
#  adapt the friction factor that was provided in the lab manual.  Charlie's solution adds one major assumption:
#  * Small angle approximation, resulting in a purely proportional relationship between servo torque and moment
#    experienced by the platform.
#
#  Plots for Part 3a:
#  <CENTER><table>
#  <tr><td>@image html 2021_05_08_07-02-10_PM-x-vs-time.png </td>
#      <td>@image html 2021_05_08_07-02-10_PM-x_dot-vs-time.png </td></tr>
#  <tr><td>@image html 2021_05_08_07-02-10_PM-theta_y-vs-time.png </td>
#      <td>@image html 2021_05_08_07-02-10_PM-theta_y_dot-vs-time.png </td></tr>
#  </table></CENTER>
#  As the ball is located directly above the CG of the platform, it is expected that with no input force, the system
#  should not move.  The plots for this first test agree with the intuition that the system shouldn't move.
#
#  Plots for Part 3b:
#  <CENTER><table>
#  <tr><td>@image html 2021_05_08_07-02-15_PM-x-vs-time.png </td>
#      <td>@image html 2021_05_08_07-02-15_PM-x_dot-vs-time.png </td></tr>
#  <tr><td>@image html 2021_05_08_07-02-15_PM-theta_y-vs-time.png </td>
#      <td>@image html 2021_05_08_07-02-15_PM-theta_y_dot-vs-time.png </td></tr>
#  </table></CENTER>
#  With the all located at the edge of the platform, with no applied force we would expect the ball to roll off to the
#  edge, pulling the platform with it.  For the most part this is true according to the output response, however the
#  ball actually rolls back towards the center briefly before continuing.  I'm not sure if this is an artifact of the
#  initial condition assumption of the simulation or if it is similar to the counter-intuitive behavior of a spool being
#  pulled at just the right angle.  Either way, the bulk behavior is as expected.
#
#  Plots for Part 4:
#  <CENTER><table>
#  <tr><td>@image html 2021_05_08_08-02-13_PM-x-vs-time.png </td>
#      <td>@image html 2021_05_08_08-02-13_PM-x_dot-vs-time.png </td></tr>
#  <tr><td>@image html 2021_05_08_08-02-13_PM-theta_y-vs-time.png </td>
#      <td>@image html 2021_05_08_08-02-13_PM-theta_y_dot-vs-time.png </td></tr>
#  </table></CENTER>
#  This response is a bit confusing, as the system oscillates but at a very high amplitude.  Using the iterative
#  Eulerian method to calculate the system response, I had to use a time step of 10^-4s in order to get results that
#  were stable.  I did not run the system until it settled, but it does not appear to follow the regulating pattern it
#  should.  Because I had to use such a small time step, solution times for simulations longer that 20sec were in the
#  range of 15-20 minutes.  In general I would say that assuming the simulation is accurate, either a smaller time step
#  is necessary to account for instability problems in this time regime, or that the controller gains need to be
#  adjusted.
#
#  @author Tyler Olson
#
#  @date May 8, 2021
#

## @page pg_hw5 Homework0x05
#  Homework 0x05: Full State Feedback
#  * Jupyter Notebook: <a href="Homework5.ipynb" target="_blank"><b>Click Here</b></a>
#  * Static HTML: <a href="Homework5.html" target="_blank"><b>Click Here</b></a>.
#
#  To complete the homework series leading up to the final project, in this last step a controller is implemented
#  on the system to achieve the desired system response.  Using the Full State Feedback strategy, the characteristic
#  polynomial is solved for the closed-loop system.  The desired characteristic polynomial is also calculated, and
#  system gains are determined by solving the resulting system of equations.
#
#  The desired system exhibits a settling time of around 1s with some overshoot acceptable.  Because a smaller settling
#  time is preferred, a damping coefficient of sqrt(1/2) was chosen as this is optimal for a PT2 system.
#  Because the system is 4th order, poles were selected so that there is a dominating complex PT2 system along with two
#  significantly larger poles, chosen arbitrarily around 10x greater than the complex poles.
#
#  The new characteristic polynomial of the system was verified (see notebook) and then simulated with the same
#  conditions used in \ref pg_hw4 .  Below are the results:
#
#  Plots from old \ref pg_hw4 controller response:
#  <CENTER><table>
#  <tr><td>@image html 2021_05_15_03-06-13_PM-x-vs-time.png </td>
#      <td>@image html 2021_05_15_03-06-13_PM-x_dot-vs-time.png </td></tr>
#  <tr><td>@image html 2021_05_15_03-06-13_PM-theta_y-vs-time.png </td>
#      <td>@image html 2021_05_15_03-06-13_PM-theta_y_dot-vs-time.png </td></tr>
#  </table></CENTER>
#  Note that the solution from \ref pg_hw4 appears different as I found an error there and fixed it for this assignment.
#  I was using the wrong units for the friction coefficient, causing the system to be unstable.
#
#  Plots from new controller response:
#  <CENTER><table>
#  <tr><td>@image html 2021_05_15_03-06-07_PM-x-vs-time.png </td>
#      <td>@image html 2021_05_15_03-06-07_PM-x_dot-vs-time.png </td></tr>
#  <tr><td>@image html 2021_05_15_03-06-07_PM-theta_y-vs-time.png </td>
#      <td>@image html 2021_05_15_03-06-07_PM-theta_y_dot-vs-time.png </td></tr>
#  </table></CENTER>
#  Note: The time scale for these plots is much smaller to show more detail.
#
#  @author Tyler Olson
#
#  @date May 15, 2021
#