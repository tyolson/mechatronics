## \addtogroup Lab0x04
#  @{

""" @file mcp9808.py
    @brief A driver for interacting with the mcp9808 Temperature Sensor using I2C.
    @details If this module is run directly, it will run a test script which prints the current temperature to the
             console once every second for one minute.
    @author Tyler Olson
    @date May 13, 2021
"""

from pyb import I2C

## @brief I2C bus address of MCP9808
#  @details Assume that A0-A2 are tied to ground.
#
BUS_ADDRESS = 0b0011000

## @brief Temperature measurement register
#
TA_REGISTER = 0b0101

## @brief Manufacturer ID register
#  @details This Register should contain the number 0b01010100
#
ID_REGISTER = 0b0110

## @brief Temperature measurement register
#
MANUFACTURER_ID = 0b01010100


class TempSensor(I2C):
    """
    @brief A Driver for the mcp9808 temperature sensor.
    @details The I2C device address can be specified by changing the global BUS_ADDRESS variable.  This wrapper
             subclasses the pyb.I2C object and accesses the mcp9808 as the master device.
    """

    def __init__(self, master=None, address=BUS_ADDRESS):
        """
        @brief Construct a TempSensor object.
        @param master Optionally wrap an existing I2C.MASTER object.
        @param address Optionally specify the I2C bus address of the mcp9808.
        """

        if master:
            super().__init__()
            self.__dict__ = master.__dict__
        else:
            super().__init__(1, I2C.MASTER)
        self.address = address
        if not self.check():
            raise RuntimeError("No Temperature Sensor detected")

    def check(self):
        """
        @brief Checks if the mcp9808 exists at the specified address and that the manufacturer id matches.
        """
        try:
            return int.from_bytes(self.mem_read(2, self.address, ID_REGISTER), 'big') == MANUFACTURER_ID
        except OSError:
            return False

    def celsius(self):
        """
        @brief Read the temperature in °C.
        @return The temperature in °C with a resolution of 0.0625°C
        """
        return self._read()

    def fahrenheit(self):
        """
        @brief Read the temperature in °F.
        @return The temperature in °F with a resolution of 0.1125°F
        """
        return 9/5 * self._read() + 32

    def _read(self):
        """
        @brief Read the temperature in °C.
        @details Polls the mcp9808 sensor and converts the returned bytes into a temperature.
        @return The temperature in °C with a resolution of 0.0625°C.
        """
        buff = self.mem_read(2, BUS_ADDRESS, TA_REGISTER)

        # mask out flag bits
        upper_byte = buff[0] & 0x1F
        lower_byte = buff[1]

        # check positive or negative
        if (upper_byte & 0x10) == 0x10:
            return 256 - (((upper_byte & 0x0F) << 4) + lower_byte / 16)
        else:
            return (upper_byte << 4) + lower_byte / 16

## @}
#


if __name__ == '__main__':
    print("Running Tests")

    import micropython, utime, pyb
    from pyb import Timer
    # Increase buffer for more helpful debugging
    micropython.alloc_emergency_exception_buf(100)

    # Stop sending debug data to UART and define the serial port to write to
    #pyb.repl_uart(None)
    #serial = pyb.UART(2)

    temp_sensor = TempSensor()
    epoch = utime.ticks_ms()

    def print_temp(_):
        print(temp_sensor.fahrenheit())

    def print_for_minute(timer):
        if (utime.ticks_diff(utime.ticks_ms(), epoch)) // 1000 < 60:
            micropython.schedule(print_temp, None)
        else:
            timer.callback(None)

    print("Starting Timer")
    my_timer = Timer(2, freq=1, callback=print_for_minute)
    while True:
        pass
