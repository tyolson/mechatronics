## \addtogroup Lab0x04 Lab0x04
#  @{

""" @file lab4/main.py
    @brief Log I2C sensor and core temperature values once a minute until stopped.
    @details Writes a log file at temps_log.csv with temperatures roughly once a minute.
    @author Tyler Olson
    @date May 13, 2021
"""

import micropython, utime, pyb
from pyb import Timer
from mcp9808 import TempSensor

# Increase buffer for more helpful debugging
# micropython.alloc_emergency_exception_buf(100)


def main():
    """
    @brief Collect temperature data from the nucleo and mcp9808 temperature sensor once a minute until
           terminated by the user.
    @details When the user sends a KeyboardInterrupt, exits gracefully.
    """
    print("Starting Data Collection")
    print("[ time(s) ]   core temp  ,    sensor temp   (Celsius)")

    temp_sensor = TempSensor()
    epoch = utime.ticks_ms()

    def log_temp(_):
        """
        @brief: Append one data point to the log file temps_log.csv including time, core temp, and sensor temp.
        @param _ unused
        """
        time = utime.ticks_diff(utime.ticks_ms(), epoch) / 1000
        core_temp = pyb.ADCAll(12, 0x70000).read_core_temp()
        sensor_temp = temp_sensor.celsius()
        with open("temps_log.csv", "a") as log:
            log.write("{},{},{}\n".format(time, core_temp, sensor_temp))
            print("[ {:^6.1f} ] core: {:<8f}, sensor: {:<8.4f} (Celsius)".format(time, core_temp, sensor_temp))

    # Start a timer to trigger once a minute
    timer = Timer(2, freq=1/60, callback=lambda _: micropython.schedule(log_temp, None))
    try:
        log_temp(None)
        while True:
            pass
    except KeyboardInterrupt:
        timer.callback(None)
        print("Testing Competed")


if __name__ == '__main__':
    main()

## @}
#