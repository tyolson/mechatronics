## \addtogroup Lab0x04
#  @{

""" @file core_temp.py
    @brief Quick script to log the board internal temperature to a file (on the Nucleo).
    @author Tyler Olson
    @date May 13, 2021
"""

import pyb

if __name__ == '__main__':
    with open("core_temp_log.txt", "a") as log:
        log.write("%f\n" % pyb.ADCAll(12, 0x70000).read_core_temp())


## @}
#