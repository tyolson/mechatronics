## \addtogroup Lab0x04
#  @{

""" @file generate_plot.py
    @brief A simple tool to plot temperatures and times collected from the Nucleo and mcp9808 temperature sensor.
    @details This script can be run either on the Nucleo or on the PC, however the datetime library is unsupported on
             the microprocessor, so it should be commented out and replaced with a call to utime.time().
    @author Tyler Olson
    @date May 13, 2021
"""
from datetime import datetime
from matplotlib import pyplot

## @}
#

if __name__ == '__main__':
    file_time = datetime.now().strftime("%Y_%m_%d_%I-%M-%S_%p")

    times = []
    core_temps = []
    sensor_temps = []

    # format data
    with open("temps_log.csv", "r") as log:
        for line in log:
            print(line.strip().split(','))
            if line.count(',') < 2:
                continue

            time, core_temp, sensor_temp = line.strip().split(',')
            times.append(float(time))
            core_temps.append(float(core_temp))
            sensor_temps.append(float(sensor_temp))

    # generate plot
    pyplot.figure()
    pyplot.plot(times, core_temps, "r", label="Core Temperature")
    pyplot.plot(times, sensor_temps, "b", label="Sensor Temperature")
    pyplot.legend()
    pyplot.title("Overnight Temperatures in Perfumo Canyon (11:49pm to 8:56am)")
    pyplot.xlabel('Time (s)')
    pyplot.ylabel(r'Temperature (°C)')
    pyplot.savefig("plot{}.png".format(file_time))


